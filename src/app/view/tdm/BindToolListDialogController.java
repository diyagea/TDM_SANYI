package app.view.tdm;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import app.model.hmi.TOOLDATA;
import app.service.TDMService;
import app.util.CommonUtil;
import app.util.DialogUtil;
import app.view.base.BaseDialogController;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;

public class BindToolListDialogController extends BaseDialogController {
	private TOOLDATA toolData = null;
	@FXML
	private TextField machineIDText;
	@FXML
	private TextField toolIDText;
	@FXML
	private TextField datetimeText;
	@FXML
	private ComboBox<String> listChooser;

	@Override
	protected void initialize() {
	}

	@Override
	public void setDataMap(Map<String, Object> dataMap) {
		this.dataMap = dataMap;
		//1.Get Transfer Data(TOOLDATA)
		toolData = (TOOLDATA) dataMap.get("TOOLDATA");
		if (toolData != null) {
			//2.load related toolListIDs to listChooser
			listChooser.getItems().addAll(TDMService.me.getToolListIDs(toolData.getTOOLID()));
			//3.set UI data
			machineIDText.setText(toolData.getMACHINEID());
			toolIDText.setText(toolData.getTOOLID());
			datetimeText.setText(CommonUtil.formatDate(toolData.getDATETIME()));
		}

	}

	@FXML
	protected void handleOk() {
		//1.check input UI data
		if (listChooser.getValue() == null) {
			DialogUtil.errorDialog("未选择绑定刀具列表！", mainApp.getLogo(), dialogStage);
			return;
		}
		String listID = listChooser.getValue();
		//2.check&load  ToolListID, TNumber, TechNo DefineLife
		dataMap.clear();
		if (!TDMService.me.loadToolData(listID, toolData.getTOOLID(), dataMap)) {
			//Error Msg
			if (dataMap.get("ERROR") != null) {
				DialogUtil.errorDialog("绑定刀具列表失败！原因：" + dataMap.get("ERROR"), mainApp.getLogo(), dialogStage);
				return;
			}

			//multiple Tool Pos
			if (dataMap.get("POSLIST") != null) {
				@SuppressWarnings("unchecked")
				List<String> posList = (List<String>) dataMap.get("POSLIST");
				String chosenPos = DialogUtil.choiceDialog("选择", "刀具列表中包含多个刀具[" + toolData.getTOOLID() + "]的数据定义", "请选择刀具在刀具列表对应的位置！", posList, null, dialogStage);
				if (chosenPos == null) {
					return;
				} else {
					dataMap.clear();
					//load ToolData
					if (!TDMService.me.loadToolData(listID, toolData.getTOOLID(), chosenPos, dataMap)) {
						//Error Msg
						if (dataMap.get("ERROR") != null) {
							DialogUtil.errorDialog("绑定刀具列表失败！原因：" + dataMap.get("ERROR"), mainApp.getLogo(), dialogStage);
							return;
						}
					} else {
						//3.set TOOLDATA Data and update database
						String toolNumber = (String) dataMap.get("TOOLNUMBER");
						String toolLife = (String) dataMap.get("TOOLLIFE");
						String warningLife = (String) dataMap.get("WARNINGLIFE");
						toolData.setTOOLNUMBER(toolNumber);
						toolData.setDEFINELIFE(new BigDecimal(toolLife));
						toolData.setREALLIFE(new BigDecimal(toolLife));
						toolData.setWARNINGLIFE(new BigDecimal(warningLife));
						if (!TDMService.me.updateToolData(toolData, dataMap)) {
							DialogUtil.errorDialog("数据库保存失败!", mainApp.getLogo(), dialogStage);
							return;
						} else {
							okClicked = true;
							dialogStage.close();
						}
					}
				}
			}
		} else {
			//3.set TOOLDATA Data and update database
			String toolNumber = (String) dataMap.get("TOOLNUMBER");
			String toolLife = (String) dataMap.get("TOOLLIFE");
			String warningLife = (String) dataMap.get("WARNINGLIFE");
			toolData.setTOOLNUMBER(toolNumber);
			toolData.setDEFINELIFE(new BigDecimal(toolLife));
			toolData.setREALLIFE(new BigDecimal(toolLife));
			toolData.setWARNINGLIFE(new BigDecimal(warningLife));
			if (!TDMService.me.updateToolData(toolData, dataMap)) {
				DialogUtil.errorDialog("数据库保存失败!", mainApp.getLogo(), dialogStage);
				return;
			} else {
				okClicked = true;
				dialogStage.close();
			}
		}

	}

	@FXML
	protected void handleCancel() {
		okClicked = false;
		dialogStage.close();
	}

}
