package app.util;

import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import javax.sql.DataSource;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.CharsetUtil;
import cn.hutool.log.Log;
import cn.hutool.log.LogFactory;
import cn.hutool.setting.Setting;

/**
 * Common Util Class
 * 
 * @author diyagea
 *
 */
public class CommonUtil {

	//Logger
	private static final Log logger = LogFactory.get();
	private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	/**
	 * load url web page (in desktop broswer)
	 * 
	 * @param url
	 */
	public static void loadWebpage(String url) {
		try {
			Desktop.getDesktop().browse(new URI(url));
		} catch (IOException | URISyntaxException e1) {
			logger.error(e1);
		}
	}

	/**
	 * Generate GUID (Globally Unique Identifier)
	 * 
	 * @return
	 */
	public static String generateGUID() {
		return UUID.randomUUID().toString().toUpperCase().replaceAll("-", "");
	}

	/*
	 * get Unix Timestamp
	 */
	public static long getUnixTimestamp() {
		return System.currentTimeMillis() / 1000;
	}

	/*
	 * get Date String(yyyy-MM-dd hh:mm:ss) by timestamp
	 */
	public static String getDateStrByTimestamp(long timestamp) {
		return DateUtil.date(timestamp * 1000).toString();
	}

	/*
	 * get Date String(yyyy-MM-dd hh:mm:ss) by timestamp
	 */
	public static String getDateStrByTimestamp(String timestamp) {
		long times = Convert.toLong(timestamp, 1514736000L);
		return DateUtil.date(times * 1000).toString();
	}

	/*
	 * get Date by timestamp
	 */
	public static Date getDateByTimestamp(String timestamp) {
		long times = Convert.toLong(timestamp, 1514736000L);
		return DateUtil.date(times * 1000);
	}

	/*
	 * get Timestamp by Date String
	 */
	public static long getTimestampByDateStr(String dateStr) {
		return DateUtil.parse(dateStr).getTime() / 1000;
	}

	/*
	 * Format Date to String (yyyy-MM-dd HH:mm:ss)
	 */
	public static String formatDate(Date date) {
		if (date == null)
			return "";
		return sdf.format(date);
	}

	/**
	 * Get Project Absolute Root Path(eg->E:/workspace/Smartool)
	 * 
	 * @return Root Path
	 */
	public static String getRootPath() {
		return System.getProperty("user.dir").replace("\\", "/");
	}

	/**
	 * Get Setting Object
	 * 
	 * @param name Setting file name.
	 * @return Setting
	 */
	public static Setting getSetting(String name) {
		return new Setting(FileUtil.touch(getRootPath() + "/resources/config/" + name), CharsetUtil.charset(CharsetUtil.UTF_8), true);
	}

	/**
	 * Test Database Connection
	 * 
	 * @return
	 */
	public static boolean testConn(DataSource ds) {
		try {
			ds.getConnection();
		} catch (Exception e) {
			logger.error(e, "Database Connection Fail!");
			return false;
		}
		return true;
	}

}
