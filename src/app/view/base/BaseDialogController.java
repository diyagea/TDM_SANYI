package app.view.base;

import java.util.Map;

import app.MainApp;
import javafx.fxml.FXML;
import javafx.stage.Stage;

/**
 * FXML Dialog Base Controller
 * 
 * @author diyagea
 *
 */
public abstract class BaseDialogController {
	protected MainApp mainApp;

	protected Map<String, Object> dataMap;

	protected Stage dialogStage;

	protected boolean okClicked = false;

	protected static final String _ACTION_NEW = "NEW";
	protected static final String _ACTION_UPDATE = "UPDATE";

	/**
	 * Returns true if the user clicked OK, false otherwise.
	 */
	public boolean isOkClicked() {
		return okClicked;
	}

	/**
	 * init controls
	 */
	@FXML
	protected abstract void initialize();

	/**
	 * transfer data from view to dialog
	 * 
	 * @param dataMap
	 */
	public abstract void setDataMap(Map<String, Object> dataMap);

	/**
	 * Called when the user clicks ok.
	 * 
	 * okClicked = true; do something... dialogStage.close();
	 * 
	 */
	@FXML
	protected abstract void handleOk();

	/**
	 * Called when the user clicks cancel.
	 * 
	 * okClicked = false; do something... dialogStage.close();
	 * 
	 */
	@FXML
	protected abstract void handleCancel();

	/*** Set&Get ***/
	public void setMainApp(MainApp mainApp) {
		this.mainApp = mainApp;
	}

	/** Dialog Stage **/
	public void setDialogStage(Stage dialogStage) {
		this.dialogStage = dialogStage;
	}

}
