package app.model.generator;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.jfinal.kit.PathKit;
import com.jfinal.plugin.activerecord.dialect.SqlServerDialect;
import com.jfinal.plugin.activerecord.generator.Generator;
import com.jfinal.plugin.druid.DruidPlugin;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;

/**
 * 本 demo 仅表达最为粗浅的 jfinal 用法，更为有价值的实用的企业级用法 详见 JFinal 俱乐部: http://jfinal.com/club
 * 
 * 在数据库表有任何变动时，运行一下 main 方法，极速响应变化进行代码重构
 */
public class _ModelGenerator {
	static DataSource dataSource;

	public static void loadDataSource() {
		//Setting setting = CommonUtil.getSetting("config.setting");
		//String jdbcUrl = "jdbc:sqlserver://" + setting.getStr("url") + ":" + setting.getStr("port") + ";databaseName=" + setting.getStr("database");
		//DruidPlugin druidPlugin = new DruidPlugin(jdbcUrl, setting.getStr("username"), setting.getStr("password"));
		String jdbcUrl = "jdbc:sqlserver://127.0.0.1:1433;databaseName=TDM_DEV";
		DruidPlugin druidPlugin = new DruidPlugin(jdbcUrl, "tms", "tms");
		druidPlugin.start();
		dataSource = druidPlugin.getDataSource();
	}

	public static void main(String[] args) {
		loadDataSource();
		// base model 所使用的包名
		String baseModelPackageName = "app.model.base";
		// base model 文件保存路径
		String baseModelOutputDir = PathKit.getRootClassPath() + "/../src/app/model/base";
		// model 所使用的包名 (MappingKit 默认使用的包名)
		String modelPackageName = "app.model";
		// mappingKit dir
		String modelOutputDir = PathKit.getRootClassPath() + "/../src/app/model";

		/*****************************************************/
		// 排除不声称的Table
		//String[] excludedTableArray = { "adv", "trace_xe_action_map", "trace_xe_event_map" };
		List<String> excludedTableList = new ArrayList<>();

		/*** Base Data ***/
		excludedTableList.add("SYS_MENU");
		excludedTableList.add("SYS_USER");
		excludedTableList.add("SYS_USERMENU");
		excludedTableList.add("DEV_ISSUEDTOOL");
		excludedTableList.add("MTM_MACHINETOOL");
		//excludedTableList.add("HMI_TOOLDATA");
		//excludedTableList.add("HMI_TOOLDATALOG");

		/*** 模块名&表前缀（*必填*） ***/
		String moduleName = "hmi";

		/*****************************************************/

		// 需要移除的Table前缀
		//String tablePrefixesRemove = "TDM_";
		String tablePrefixesRemove = moduleName.toUpperCase() + "_";
		String[] excludedTableArray = new String[excludedTableList.size()];
		//do generate
		generate(moduleName, baseModelPackageName, baseModelOutputDir, modelPackageName, modelOutputDir, tablePrefixesRemove, excludedTableList.toArray(excludedTableArray));
	}

	private static void generate(String moduleName, String baseModelPackageName, String baseModelOutputDir, String modelPackageName, String modelOutputDir, String tablePrefixesRemove, String... excludedTable) {

		// model 文件保存路径 (MappingKit 与 DataDictionary 文件默认保存路径)
		String mappingKitDir = modelOutputDir;
		String mappingKitPackageName = modelPackageName;

		//模块名称
		if (StrUtil.isNotBlank(moduleName)) {
			baseModelPackageName += "." + moduleName;
			baseModelOutputDir += "/" + moduleName;
			modelPackageName += "." + moduleName;
			modelOutputDir += "/" + moduleName;
		}

		// 创建生成器
		Generator generator = new Generator(dataSource, baseModelPackageName, baseModelOutputDir, modelPackageName, modelOutputDir);

		//set MappingKit
		generator.setMappingKitPackageName(mappingKitPackageName);
		generator.setMappingKitOutputDir(mappingKitDir);
		generator.setMappingKitClassName("_MappingKit_" + DateUtil.today());

		//设置数据库方言
		generator.setDialect(new SqlServerDialect());
		// SqlMetaBuilder
		generator.setMetaBuilder(new _SqlMetaBuilder(dataSource));
		// 设置是否在 Model 中生成 dao 对象
		generator.setGenerateDaoInModel(true);
		// 设置是否生成链式 setter 方法
		generator.setGenerateChainSetter(true);
		// 设置是否生成字典文件
		generator.setGenerateDataDictionary(false);
		// 设置需要被移除的表名前缀用于生成modelName。例如表名 "osc_user"，移除前缀 "osc_"后生成的model名为 "User"而非 OscUser
		generator.setRemovedTableNamePrefixes(tablePrefixesRemove);
		// 添加不需要生成的表名
		generator.addExcludedTable(excludedTable);
		// 生成
		generator.generate();
	}
}
