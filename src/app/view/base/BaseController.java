package app.view.base;

import java.util.HashMap;
import java.util.Map;

import app.MainApp;
import javafx.fxml.FXML;

/**
 * FXML View Base Controller
 * @author diyagea
 *
 */
public abstract class BaseController {
	protected MainApp mainApp;

	protected Map<String, Object> dataMap = new HashMap<>();
	
	/**
	 * init controls
	 */
	@FXML
	protected abstract void initialize();

	public void setMainApp(MainApp mainApp) {
		this.mainApp = mainApp;
	}
	
	public void setDataMap(Map<String, Object> dataMap){
		this.dataMap = dataMap;
	}
}
