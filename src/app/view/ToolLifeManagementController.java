package app.view;

import java.io.File;
import java.math.BigDecimal;
import java.time.LocalDate;

import app.model.hmi.TOOLDATA;
import app.model.hmi.TOOLDATALOG;
import app.service.TDMService;
import app.util.CommonUtil;
import app.util.DialogUtil;
import app.util.PDFUtil;
import app.util.PathUtil;
import app.util.QRCodeUtil;
import app.util.ValidatorUtil;
import app.view.base.BaseController;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToolBar;

public class ToolLifeManagementController extends BaseController {
	@FXML
	ToolBar toolBar;
	@FXML
	Button inputBtn;
	@FXML
	Button bindBtn;
	@FXML
	Button printBtn;
	@FXML
	Button scanLifeCardBtn;
	@FXML
	Button resetToolLifeBtn;
	@FXML
	Tab outTab;
	@FXML
	Tab inTab;
	@FXML
	Tab logTab;
	@FXML
	ComboBox<String> machineChooser;
	@FXML
	TextField toolInfoText;
	@FXML
	DatePicker startTime;
	@FXML
	DatePicker endTime;
	@FXML
	Button searchBtn;
	@FXML
	Button clearBtn;
	@FXML
	private TableView<TOOLDATA> issuedToolTable;
	@FXML
	private TableColumn<TOOLDATA, String> machineIDCol;
	@FXML
	private TableColumn<TOOLDATA, String> toolIDCol;
	@FXML
	private TableColumn<TOOLDATA, String> toolNameCol;
	@FXML
	private TableColumn<TOOLDATA, String> toolNumberCol;
	@FXML
	private TableColumn<TOOLDATA, String> defineLifeCol;
	@FXML
	private TableColumn<TOOLDATA, String> warningLifeCol;
	@FXML
	private TableColumn<TOOLDATA, String> realLifeCol;
	@FXML
	private TableColumn<TOOLDATA, String> datetimeCol;
	@FXML
	private TableView<TOOLDATA> returnToolTable;
	@FXML
	private TableColumn<TOOLDATA, String> machineID2Col;
	@FXML
	private TableColumn<TOOLDATA, String> toolID2Col;
	@FXML
	private TableColumn<TOOLDATA, String> toolName2Col;
	@FXML
	private TableColumn<TOOLDATA, String> toolNumber2Col;
	@FXML
	private TableColumn<TOOLDATA, String> defineLife2Col;
	@FXML
	private TableColumn<TOOLDATA, String> warningLife2Col;
	@FXML
	private TableColumn<TOOLDATA, String> realLife2Col;
	@FXML
	private TableColumn<TOOLDATA, String> datetime2Col;
	@FXML
	private TableView<TOOLDATALOG> logTable;
	@FXML
	private TableColumn<TOOLDATALOG, String> logMachineIDCol;
	@FXML
	private TableColumn<TOOLDATALOG, String> logToolIDCol;
	@FXML
	private TableColumn<TOOLDATALOG, String> logTypeCol;
	@FXML
	private TableColumn<TOOLDATALOG, String> logNoteCol;
	@FXML
	private TableColumn<TOOLDATALOG, String> logTimeCol;

	@Override
	protected void initialize() {
		machineChooser.getItems().addAll(TDMService.me.getMachineIDs());
		inputBtn.disableProperty().bind(inTab.selectedProperty());
		bindBtn.disableProperty().bind(inTab.selectedProperty());
		scanLifeCardBtn.disableProperty().bind(inTab.selectedProperty());
		printBtn.disableProperty().bind(outTab.selectedProperty());
		toolBar.disableProperty().bind(logTab.selectedProperty());

		machineIDCol.setCellValueFactory(cellData -> cellData.getValue().machineIDProperty());
		toolIDCol.setCellValueFactory(cellData -> cellData.getValue().toolIDProperty());
		toolNameCol.setCellValueFactory(cellData -> cellData.getValue().toolNameProperty());
		toolNumberCol.setCellValueFactory(cellData -> cellData.getValue().toolNumProperty());
		defineLifeCol.setCellValueFactory(cellData -> cellData.getValue().defineLifeProperty());
		warningLifeCol.setCellValueFactory(cellData -> cellData.getValue().warningLifeProperty());
		realLifeCol.setCellValueFactory(cellData -> cellData.getValue().realLifeProperty());
		datetimeCol.setCellValueFactory(cellData -> cellData.getValue().datetimeProperty());
		machineID2Col.setCellValueFactory(cellData -> cellData.getValue().machineIDProperty());
		toolID2Col.setCellValueFactory(cellData -> cellData.getValue().toolIDProperty());
		toolName2Col.setCellValueFactory(cellData -> cellData.getValue().toolNameProperty());
		toolNumber2Col.setCellValueFactory(cellData -> cellData.getValue().toolNumProperty());
		defineLife2Col.setCellValueFactory(cellData -> cellData.getValue().defineLifeProperty());
		warningLife2Col.setCellValueFactory(cellData -> cellData.getValue().warningLifeProperty());
		realLife2Col.setCellValueFactory(cellData -> cellData.getValue().realLifeProperty());
		datetime2Col.setCellValueFactory(cellData -> cellData.getValue().datetimeProperty());
		logMachineIDCol.setCellValueFactory(cellData -> cellData.getValue().machineIDProperty());
		logToolIDCol.setCellValueFactory(cellData -> cellData.getValue().toolIDProperty());
		logTypeCol.setCellValueFactory(cellData -> cellData.getValue().typeProperty());
		logNoteCol.setCellValueFactory(cellData -> cellData.getValue().noteProperty());
		logTimeCol.setCellValueFactory(cellData -> cellData.getValue().datetimeProperty());

		issuedToolTable.getItems().addAll(TDMService.me.getIssueOutToolDatas(null, 0, null, null, null));
		outTab.selectedProperty().addListener(new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				if (newValue) {
					handleSearch();
				}
			}
		});
		inTab.selectedProperty().addListener(new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				if (newValue) {
					handleSearch();
				}
			}
		});
		logTab.selectedProperty().addListener(new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				if (newValue) {
					handleSearch();
				}
			}
		});

	}

	@FXML
	public void handleSearch() {
		String machineInfo = machineChooser.getEditor().getText();
		String toolInfo = toolInfoText.getText();
		LocalDate startDate = startTime.getValue();
		LocalDate endDate = endTime.getValue();
		if (outTab.isSelected()) {
			issuedToolTable.getItems().clear();
			issuedToolTable.getItems().addAll(TDMService.me.getIssueOutToolDatas(machineInfo, 0, toolInfo, startDate, endDate));
		} else if (inTab.isSelected()) {
			returnToolTable.getItems().clear();
			returnToolTable.getItems().addAll(TDMService.me.getIssueOutToolDatas(machineInfo, 1, toolInfo, startDate, endDate));
		} else if (logTab.isSelected()) {
			logTable.getItems().clear();
			logTable.getItems().addAll(TDMService.me.queryToolDataLogs(machineInfo, toolInfo, startDate, endDate));
		}
	}

	@FXML
	private void handleClear() {
		machineChooser.setValue(null);
		toolInfoText.clear();
		startTime.setValue(null);
		endTime.setValue(null);
		handleSearch();
	}

	@FXML
	private void handleInput() {
		TOOLDATA toolData = issuedToolTable.getSelectionModel().getSelectedItem();
		if (toolData == null) {//no select data
			DialogUtil.errorDialog("未选择刀具数据！", mainApp.getLogo(), mainApp.getPrimaryStage());
			return;
		}
		dataMap.clear();
		dataMap.put("TOOLDATA", toolData);
		if (mainApp.loadFxmlDialog("tdm", "InputToolDataDialog.fxml", "手动填写刀具信息", dataMap)) {
			TDMService.me.addLog(1, toolData.getMACHINEID(), toolData.getTOOLID(), "手动输入刀具信息：T号='" + toolData.getTOOLNUMBER() + "'，名义寿命='" + toolData.getDEFINELIFE() + "'，实际寿命='" + toolData.getREALLIFE() + "'");
			DialogUtil.successNotification("刀具[" + toolData.getTOOLID() + "]设置成功！", mainApp.getPrimaryStage());
			handleSearch();
		}
	}

	@FXML
	private void handleBindToolList() {
		TOOLDATA toolData = issuedToolTable.getSelectionModel().getSelectedItem();
		if (toolData == null) {//no select data
			DialogUtil.errorDialog("未选择刀具数据！", mainApp.getLogo(), mainApp.getPrimaryStage());
			return;
		}
		dataMap.clear();
		dataMap.put("TOOLDATA", toolData);
		if (mainApp.loadFxmlDialog("tdm", "BindToolListDialog.fxml", "绑定刀具列表", dataMap)) {
			TDMService.me.addLog(2, toolData.getMACHINEID(), toolData.getTOOLID(), "绑定刀具清单中的刀具信息：T号='" + toolData.getTOOLNUMBER() + "'，名义寿命='" + toolData.getDEFINELIFE() + "'，实际寿命='" + toolData.getREALLIFE() + "'");
			DialogUtil.successNotification("刀具[" + toolData.getTOOLID() + "]绑定成功！", mainApp.getPrimaryStage());
			handleSearch();
		}
	}

	@FXML
	private void handleScanToolLifeCard() {
		//get data from table
		TOOLDATA toolData = issuedToolTable.getSelectionModel().getSelectedItem();
		if (toolData == null) {//no select data
			DialogUtil.errorDialog("未选择刀具数据！", mainApp.getLogo(), mainApp.getPrimaryStage());
			return;
		}

		if (toolData.getDEFINELIFE() == null) {
			DialogUtil.errorDialog("请先绑定刀具清单，加载刀具预定义寿命！", mainApp.getLogo(), mainApp.getPrimaryStage());
			return;
		}

		//scan life card
		String guid = DialogUtil.inputDialog("领取旧刀具 - 寿命卡扫码", "扫码：", true, mainApp.getLogo(), mainApp.getPrimaryStage());
		if (guid != null) {
			TOOLDATA oldToolData = TDMService.me.queryToolDataByGUID(guid);
			if (oldToolData == null) {
				DialogUtil.errorDialog("扫码失败！未找到相应历史数据！", mainApp.getLogo(), mainApp.getPrimaryStage());
				return;
			}

			if (!oldToolData.getItemID().equals(toolData.getItemID())) {
				DialogUtil.errorDialog("刀具单项寿命卡不匹配！\n单项ID[" + oldToolData.getItemID() + "]不能存在刀具组装[" + toolData.getTOOLID() + "]中！", mainApp.getLogo(), mainApp.getPrimaryStage());
				return;
			}

			toolData.setREALLIFE(oldToolData.getREALLIFE());

			//update database
			if (TDMService.me.updateToolData(toolData, dataMap)) {
				TDMService.me.addLog(3, toolData.getMACHINEID(), toolData.getTOOLID(), "领取使用旧刀具：单项ID='" + oldToolData.getItemID() + "'，实际寿命='" + oldToolData.getREALLIFE() + "'");
				DialogUtil.successNotification("数据已更新！", mainApp.getPrimaryStage());
				handleSearch();//reload data
			} else {
				if (dataMap.get("ERROR") != null) {
					DialogUtil.errorDialog("操作失败!原因：" + dataMap.get("ERROR"), mainApp.getLogo(), mainApp.getPrimaryStage());
				}
			}
		}
	}

	@FXML
	private void handlePrintToolData() {
		TOOLDATA toolData = returnToolTable.getSelectionModel().getSelectedItem();
		if (toolData == null) {//no select data
			DialogUtil.errorDialog("未选择刀具数据！", mainApp.getLogo(), mainApp.getPrimaryStage());
			return;
		}
		if (ValidatorUtil.isEmpty(toolData.getGUID())) {
			String guid = CommonUtil.generateGUID();
			toolData.setGUID(guid);
			//toolData.setISREAD(1);
		}
		if (TDMService.me.updateToolData(toolData, dataMap)) {
			String outPutPath = mainApp.getSetting().getStr("LifeCardPath");
			if (ValidatorUtil.isEmpty(outPutPath)) {
				outPutPath = PathUtil.LifeCardPath;
			}
			outPutPath = outPutPath + File.separator + toolData.getTOOLID() + File.separator;
			if (QRCodeUtil.writeQrCodeContent(toolData.getGUID(), toolData.getGUID(), PathUtil.qrCodePath) != null) {
				if (PDFUtil.generateToolLifeCard(outPutPath, toolData.getGUID(), toolData)) {
					TDMService.me.addLog(4, toolData.getMACHINEID(), toolData.getTOOLID(), "刀具寿命卡打印：组装刀具='" + toolData.getTOOLID() + "'，实际寿命='" + toolData.getREALLIFE() + "'");
					DialogUtil.successNotification("刀具寿命卡已导出！", mainApp.getPrimaryStage());
					handleSearch();
				} else {
					DialogUtil.errorDialog("刀具寿命卡保存失败！", mainApp.getLogo(), mainApp.getPrimaryStage());
				}
			} else {
				DialogUtil.errorDialog("二维码生成失败！", mainApp.getLogo(), mainApp.getPrimaryStage());
			}
		} else {
			DialogUtil.errorDialog("操作失败！原因：" + dataMap.get("ERROR"), mainApp.getLogo(), mainApp.getPrimaryStage());
		}
	}

	@FXML
	private void handleResetToolLife() {
		//scan tool life card
		String guid = DialogUtil.inputDialog("重新设定刀具寿命", "寿命卡扫码：", true, mainApp.getLogo(), mainApp.getPrimaryStage());
		if (guid != null) {
			//query old ToolData by Guid
			TOOLDATA oldToolData = TDMService.me.queryToolDataByGUID(guid);
			if (oldToolData == null) {
				DialogUtil.errorDialog("扫码失败！未找到相应历史数据！", mainApp.getLogo(), mainApp.getPrimaryStage());
				return;
			}

			//input new life
			String newToolLife = DialogUtil.inputDialog("单件ID[" + oldToolData.getItemID() + "]\n组装刀具ID[" + oldToolData.getTOOLID() + "]\n名义寿命：[" + oldToolData.getDEFINELIFE() + "]\n预警寿命：[" + oldToolData.getWARNINGLIFE() + "]\n实际寿命：[" + oldToolData.getREALLIFE() + "]", "填写新刀具剩余寿命：", true, mainApp.getLogo(), mainApp.getPrimaryStage());
			if (newToolLife != null) {
				if (!ValidatorUtil.isNumber(newToolLife)) {
					DialogUtil.errorDialog("刀具寿命输入格式错误，请输入正确的刀具寿命！", mainApp.getLogo(), mainApp.getPrimaryStage());
					return;
				}
				BigDecimal newRealLife = new BigDecimal(newToolLife);
				if (oldToolData.getDEFINELIFE() != null) {
					if (oldToolData.getDEFINELIFE().compareTo(newRealLife) < 0) {
						DialogUtil.errorDialog("刀具剩余寿命不能大于预定义寿命，请输入正确的刀具寿命！", mainApp.getLogo(), mainApp.getPrimaryStage());
						return;
					}
				}
				//set new life
				oldToolData.setREALLIFE(newRealLife);
				//update database
				if (TDMService.me.updateToolData(oldToolData, dataMap)) {
					TDMService.me.addLog(5, oldToolData.getMACHINEID(), oldToolData.getTOOLID(), "重设刀具寿命信息：组装刀具='" + oldToolData.getTOOLID() + "'，实际寿命='" + oldToolData.getREALLIFE() + "'");
					DialogUtil.successNotification("数据已更新！", mainApp.getPrimaryStage());
					handleSearch();//reload data
				} else {
					if (dataMap.get("ERROR") != null) {
						DialogUtil.errorDialog("操作失败! 原因：" + dataMap.get("ERROR"), mainApp.getLogo(), mainApp.getPrimaryStage());
					}
				}
			}
		}
	}
}
