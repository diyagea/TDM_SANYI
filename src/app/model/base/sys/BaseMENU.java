package app.model.base.sys;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.IBean;

/**
 * Generated by JFinal, do not modify this file.
 */
@SuppressWarnings({"serial", "unchecked"})
public abstract class BaseMENU<M extends BaseMENU<M>> extends Model<M> implements IBean {

	public M setMENUID(java.lang.Integer MENUID) {
		set("MENUID", MENUID);
		return (M)this;
	}

	public java.lang.Integer getMENUID() {
		return getInt("MENUID");
	}

	public M setPID(java.lang.Integer PID) {
		set("PID", PID);
		return (M)this;
	}

	public java.lang.Integer getPID() {
		return getInt("PID");
	}

	public M setMENUINDEX(java.lang.String MENUINDEX) {
		set("MENUINDEX", MENUINDEX);
		return (M)this;
	}

	public java.lang.String getMENUINDEX() {
		return getStr("MENUINDEX");
	}

	public M setMENUNAME(java.lang.String MENUNAME) {
		set("MENUNAME", MENUNAME);
		return (M)this;
	}

	public java.lang.String getMENUNAME() {
		return getStr("MENUNAME");
	}

	public M setMODULE(java.lang.String MODULE) {
		set("MODULE", MODULE);
		return (M)this;
	}

	public java.lang.String getMODULE() {
		return getStr("MODULE");
	}

	public M setFXML(java.lang.String FXML) {
		set("FXML", FXML);
		return (M)this;
	}

	public java.lang.String getFXML() {
		return getStr("FXML");
	}

	public M setACCESS(java.lang.String ACCESS) {
		set("ACCESS", ACCESS);
		return (M)this;
	}

	public java.lang.String getACCESS() {
		return getStr("ACCESS");
	}

	public M setIMAGE(java.lang.String IMAGE) {
		set("IMAGE", IMAGE);
		return (M)this;
	}

	public java.lang.String getIMAGE() {
		return getStr("IMAGE");
	}

	public M setTYPE(java.lang.Integer TYPE) {
		set("TYPE", TYPE);
		return (M)this;
	}

	public java.lang.Integer getTYPE() {
		return getInt("TYPE");
	}

	public M setSTATE(java.lang.Integer STATE) {
		set("STATE", STATE);
		return (M)this;
	}

	public java.lang.Integer getSTATE() {
		return getInt("STATE");
	}

	public M setPOS(java.lang.Integer POS) {
		set("POS", POS);
		return (M)this;
	}

	public java.lang.Integer getPOS() {
		return getInt("POS");
	}

}
