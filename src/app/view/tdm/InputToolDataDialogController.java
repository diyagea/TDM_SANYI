package app.view.tdm;

import java.math.BigDecimal;
import java.util.Map;

import app.model.hmi.TOOLDATA;
import app.service.TDMService;
import app.util.DialogUtil;
import app.util.ValidatorUtil;
import app.view.base.BaseDialogController;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;

public class InputToolDataDialogController extends BaseDialogController {
	private TOOLDATA toolData = null;
	@FXML
	private TextField toolNumberText;
	@FXML
	private TextField toolIDText;
	@FXML
	private TextField defineLifeText;
	@FXML
	private TextField warningLifeText;
	@FXML
	private TextField realLifeText;

	@Override
	protected void initialize() {
	}

	@Override
	public void setDataMap(Map<String, Object> dataMap) {
		this.dataMap = dataMap;
		toolData = (TOOLDATA) dataMap.get("TOOLDATA");
		if (toolData != null) {
			toolIDText.setText(toolData.getTOOLID());
			toolNumberText.setText(toolData.getTOOLNUMBER());
			defineLifeText.setText(toolData.getDEFINELIFE() == null ? "" : toolData.getDEFINELIFE().toString());
			warningLifeText.setText(toolData.getWARNINGLIFE() == null ? "" : toolData.getWARNINGLIFE().toString());
			realLifeText.setText(toolData.getREALLIFE() == null ? "" : toolData.getREALLIFE().toString());
		}
	}

	@FXML
	protected void handleOk() {
		dataMap.clear();
		if (ValidatorUtil.isEmpty(toolNumberText.getText())) {
			DialogUtil.errorDialog("T号不能为空！", mainApp.getLogo(), dialogStage);
			return;
		}
		if (!ValidatorUtil.isNumber(realLifeText.getText()) || !ValidatorUtil.isNumber(defineLifeText.getText()) || !ValidatorUtil.isNumber(warningLifeText.getText())) {
			DialogUtil.errorDialog("输入寿命数据格式有误！", mainApp.getLogo(), dialogStage);
			return;
		}

		BigDecimal defineLife = new BigDecimal(defineLifeText.getText());
		BigDecimal realLife = new BigDecimal(realLifeText.getText());
		BigDecimal warningLife = new BigDecimal(warningLifeText.getText());

		if (realLife.compareTo(defineLife) == 1) {
			DialogUtil.errorDialog("剩余寿命不能大于名义寿命！", mainApp.getLogo(), dialogStage);
			return;
		}

		toolData.setTOOLNUMBER(toolNumberText.getText());
		toolData.setDEFINELIFE(defineLife);
		toolData.setREALLIFE(realLife);
		toolData.setWARNINGLIFE(warningLife);

		if (TDMService.me.updateToolData(toolData, dataMap)) {
			okClicked = true;
			dialogStage.close();
		} else {
			DialogUtil.errorNotification("操作失败！", mainApp.getPrimaryStage());
			if (dataMap.get("ERROR") != null) {
				DialogUtil.errorDialog("失败原因：" + dataMap.get("ERROR"), mainApp.getLogo(), dialogStage);
			}
		}
	}

	@FXML
	protected void handleCancel() {
		okClicked = false;
		dialogStage.close();
	}

}
