package app.util;

import java.io.File;
import java.util.List;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.dialect.SqlServerDialect;
import com.jfinal.plugin.druid.DruidPlugin;

import app.model._MappingKit;
import app.model.sys.MENU;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.XmlUtil;
import cn.hutool.setting.Setting;

/**
 * load MENU.xml data, import into database
 * 
 * @author diyagea
 *
 */
public class MenuImportUtil {

	public static void main(String[] args) {
		importMenuFromXML();
	}

	private static void importMenuFromXML() {
		startDataSource();
		File xmlFile = new File(XmlDataUtil._root_Path + XmlDataUtil._xml_data_path + "MENU.xml");
		Document doc = XmlUtil.readXML(xmlFile);
		Element root = XmlUtil.getRootElement(doc);
		Db.update("TRUNCATE TABLE SYS_MENU");
		MENU rootMenu = new MENU();
		rootMenu.setMENUID(Convert.toInt(root.getAttribute("MENUID")));
		rootMenu.setPID(Convert.toInt(root.getAttribute("PID")));
		rootMenu.setFXML(root.getAttribute("FXML"));
		rootMenu.setIMAGE(root.getAttribute("IMAGE"));
		rootMenu.setMENUINDEX(root.getAttribute("MENUINDEX"));
		rootMenu.setMENUNAME(root.getAttribute("MENUNAME"));
		rootMenu.setMODULE(root.getAttribute("MODULE"));
		rootMenu.setACCESS(root.getAttribute("ACCESS"));
		rootMenu.setPOS(Convert.toInt(root.getAttribute("POS"), 0));
		rootMenu.setTYPE(Convert.toInt(root.getAttribute("TYPE"), 0));
		rootMenu.setSTATE(Convert.toInt(root.getAttribute("STATE"), 0));
		rootMenu.save();
		saveSubMenuList(root);
		System.out.println("菜单导入完成！");
	}

	private static void saveSubMenuList(Element root) {
		List<Element> tagList = XmlUtil.getElements(root, "MENU");
		for (Element e : tagList) {
			MENU temp = new MENU();
			temp.setMENUID(Convert.toInt(e.getAttribute("MENUID")));
			temp.setPID(Convert.toInt(e.getAttribute("PID")));
			temp.setFXML(e.getAttribute("FXML"));
			temp.setIMAGE(e.getAttribute("IMAGE"));
			temp.setMENUINDEX(e.getAttribute("MENUINDEX"));
			temp.setMENUNAME(e.getAttribute("MENUNAME"));
			temp.setMODULE(e.getAttribute("MODULE"));
			temp.setACCESS(e.getAttribute("ACCESS"));
			temp.setPOS(Convert.toInt(e.getAttribute("POS"), 0));
			temp.setTYPE(Convert.toInt(e.getAttribute("TYPE"), 0));
			temp.setSTATE(Convert.toInt(e.getAttribute("STATE"), 0));
			temp.save();
			saveSubMenuList(e);
		}
	}

	/**
	 * Start Database Plugin
	 */
	private static void startDataSource() {
		Setting setting = CommonUtil.getSetting("config.setting");
		String jdbcUrl = "jdbc:sqlserver://" + setting.getStr("url") + ":" + setting.getStr("port") + ";databaseName=" + setting.getStr("database");
		DruidPlugin druidPlugin = new DruidPlugin(jdbcUrl, setting.getStr("username"), setting.getStr("password"));
		ActiveRecordPlugin activeRecordPlugin = new ActiveRecordPlugin(druidPlugin);
		//set SQL Dialect
		activeRecordPlugin.setDialect(new SqlServerDialect());

		//Mapping Model Object
		_MappingKit.mapping(activeRecordPlugin);

		druidPlugin.start();
		activeRecordPlugin.start();
	}

}
