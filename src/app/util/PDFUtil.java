package app.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfWriter;

import app.model.hmi.TOOLDATA;
import cn.hutool.log.Log;
import cn.hutool.log.LogFactory;

/**
 * 二维码
 * 
 * 单件ID: xxxx
 * 
 * 组装刀具ID: xxxx
 * 
 * T号： xxxx
 * 
 * 机床ID: xxxx
 * 
 * 名义寿命： xxxx
 * 
 * 剩余寿命： xxxx
 * 
 * 日期： xxxx/xx/xx xx：xx：xxxx
 * 
 * @author diyagea
 *
 */
public class PDFUtil {
	private static final Log logger = LogFactory.get();

	public static boolean generateToolLifeCard(String outPutPath, String imgName, TOOLDATA toolData) {
		try {
			File f = new File(outPutPath);
			if (!f.isDirectory() && !f.exists()) {
				f.mkdirs();
			}

			String fileName = outPutPath + "ToolLifeCard-" + System.currentTimeMillis() + ".pdf";

			//页面大小(宽,高)
			//Rectangle rect = new Rectangle(200, 350);
			Rectangle rect = new Rectangle(PageSize.A4);
			Document document = new Document(rect);
			PdfWriter.getInstance(document, new FileOutputStream(fileName));
			document.open();

			//image
			Image img = Image.getInstance(PathUtil.qrCodePath + imgName + ".png");
			img.setAlignment(Image.ALIGN_CENTER | Image.DEFAULT);
			//img.scaleToFit(100, 100);//大小
			document.add(img);

			//info
			Paragraph para = new Paragraph();
			//para.setAlignment(Element.ALIGN_JUSTIFIED);
			//para.setIndentationLeft(-15f);
			para.add(Chunk.NEWLINE);
			para.add(Chunk.NEWLINE);
			para.add(Chunk.NEWLINE);
			para.setFont(setChineseFont());
			para.add(new Chunk("单件ID： " + toolData.getItemID()));
			para.add(Chunk.NEWLINE);
			para.add(Chunk.NEWLINE);
			para.add(new Chunk("组装刀具ID：" + toolData.getTOOLID()));
			para.add(Chunk.NEWLINE);
			para.add(Chunk.NEWLINE);
			para.add(new Phrase("T号： " + toolData.getTOOLNUMBER()));
			para.add(Chunk.NEWLINE);
			para.add(Chunk.NEWLINE);
			para.add(new Phrase("机床ID：" + toolData.getMACHINEID()));
			para.add(Chunk.NEWLINE);
			para.add(Chunk.NEWLINE);
			para.add(new Phrase("名义寿命：" + toolData.getDEFINELIFE()));
			para.add(Chunk.NEWLINE);
			para.add(Chunk.NEWLINE);
			para.add(new Phrase("预警寿命：" + toolData.getWARNINGLIFE()));
			para.add(Chunk.NEWLINE);
			para.add(Chunk.NEWLINE);
			para.add(new Phrase("剩余寿命：" + toolData.getREALLIFE()));
			para.add(Chunk.NEWLINE);
			para.add(Chunk.NEWLINE);
			para.add(new Phrase("日期：" + CommonUtil.formatDate(toolData.getDATETIME())));
			para.add(Chunk.NEWLINE);
			para.add(Chunk.NEWLINE);
			document.add(para);
			document.close();

			System.out.println("完成！");

			Runtime.getRuntime().exec("rundll32 url.dll FileProtocolHandler " + fileName);

			return true;
		} catch (Exception e) {
			logger.error(e);
			return false;
		}
	}

	/**
	 * 设置中文内容Font（黑体）
	 * 
	 * @return
	 */
	public static Font setChineseFont() {
		BaseFont bf = null;
		Font fontChinese = null;
		try {
			bf = BaseFont.createFont("C:\\WINDOWS\\FONTS\\SIMHEI.TTF", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
			fontChinese = new Font(bf, 20, Font.NORMAL);
		} catch (DocumentException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return fontChinese;
	}

}
