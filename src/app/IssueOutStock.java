package app;

import java.util.Date;

import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.dialect.SqlServerDialect;
import com.jfinal.plugin.druid.DruidPlugin;

import app.model._MappingKit;
import app.model.hmi.TOOLDATA;
import app.util.CommonUtil;
import app.util.DialogUtil;
import cn.hutool.log.Log;
import cn.hutool.log.LogFactory;
import cn.hutool.setting.Setting;
import javafx.application.Application;
import javafx.stage.Stage;

/**
 * TDM 出库刀具信息插入中间表
 * 
 * 提醒：RootPath=C:/TDM 所以Resource目录要拷贝到TDM根目录下
 * 
 * @author Allen Wang
 *
 */
public class IssueOutStock extends Application {

	//Logger
	private static final Log logger = LogFactory.get();

	//Setting
	private Setting setting = null;

	//Database Plugin
	private DruidPlugin druidPlugin;

	//Main Stage
	private Stage primaryStage;

	private static String machineID;
	private static String toolID;
	private static int dataLength = 2;

	/**
	 * Application Run Main Method
	 */
	public static void main(String[] args) {
		//LauncherImpl.launchApplication(MainApp.class, SplashScreen.class, args);
		if (args != null && args.length == dataLength) {
			machineID = args[0];
			toolID = args[1];
			logger.info("Input Patameter : Machine='" + machineID + "',Tool='" + toolID + "'");
			//logger.info("Script Root Path:" + CommonUtil.getRootPath());
			launch(args);
		} else {
			logger.error("No parameter input, So do not execute method!");
			System.exit(0);
		}
	}

	/**
	 * Application Init loading Method
	 */
	@Override
	public void init() throws Exception {
		//Load setting
		setting = CommonUtil.getSetting("config.setting");
	}

	/**
	 * Application Start Method
	 */
	@Override
	public void start(Stage primaryStage) {
		try {
			//Start DB Plugin and Check DB connection 
			if (!startDBPlugin()) {
				DialogUtil.errorDialog("数据库连接失败，请检查配置文件是否正确！", null, null);
				return;
			}
			//query tool name
			String toolName = Db.queryStr("SELECT NAME FROM TDM_TOOL WHERE TOOLID=? ", toolID);

			//create ToolData model
			TOOLDATA toolData = new TOOLDATA();
			//toolData.setGUID(CommonUtil.generateGUID());
			toolData.setMACHINEID(machineID);
			toolData.setTOOLID(toolID);
			toolData.setTOOLNAME(toolName);
			toolData.setDATETIME(new Date());
			toolData.setTYPE(0);
			toolData.setISREAD(0);
			if (toolData.save()) {
				logger.info("SUCCESS");
			} else {
				logger.info("FAIL");
			}
		} catch (Exception e) {
			logger.error(e);
			return;
		} finally {
			stop();
		}
	}

	/**
	 * Application Start loading Method
	 */
	@Override
	public void stop() {
		//do something need to stop
		if (druidPlugin != null) {
			druidPlugin.stop();
		}
		System.exit(0);
	}

	/**
	 * Start Database Plugin
	 */
	public boolean startDBPlugin() {
		try {
			//init Database connection pool
			String jdbcUrl = "jdbc:sqlserver://" + setting.getStr("url") + ":" + setting.getStr("port") + ";databaseName=" + setting.getStr("database");
			druidPlugin = new DruidPlugin(jdbcUrl, setting.getStr("username"), setting.getStr("password"));
			ActiveRecordPlugin activeRecordPlugin = new ActiveRecordPlugin(druidPlugin);

			//set SQL Dialect
			activeRecordPlugin.setDialect(new SqlServerDialect());

			//Mapping Model Object
			_MappingKit.mapping(activeRecordPlugin);

			// 与 jfinal web 环境唯一的不同是要手动调用一次相关插件的start()方法
			//check database start result
			if (druidPlugin.start() && activeRecordPlugin.start()) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			logger.error(e, "Connect Database Error!");
			return false;
		}
	}

	/********************** GET&SET *************************/

	/**
	 * Get the main stage.
	 */
	public Stage getPrimaryStage() {
		return primaryStage;
	}
}