package app.util;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;
import java.util.Optional;

import cn.hutool.log.Log;
import cn.hutool.log.LogFactory;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.stage.Stage;

public class DialogUtil {

	//Logger
	@SuppressWarnings("unused")
	private static final Log logger = LogFactory.get();

	private static final ImageView writeImageView = new ImageView(FXUtil.getImg(PathUtil._IMG_PATH, "WriteNote.png"));

	/**
	 * Success Notification
	 * 
	 * @param title
	 * @param content
	 * @param owner
	 */
	public static void successNotification(String content, Stage owner) {
		ControlFXUtil.showNotification(AlertType.NONE, Pos.BOTTOM_RIGHT, "action_success.png", "信息", content, 3000, false, owner);
	}

	/**
	 * Information Notification
	 * 
	 * @param title
	 * @param content
	 * @param owner
	 */
	public static void infoNotification(String content, Stage owner) {
		ControlFXUtil.showNotification(AlertType.INFORMATION, Pos.BOTTOM_RIGHT, "信息", content, owner);
	}

	/**
	 * Warning Notification
	 * 
	 * @param title
	 * @param content
	 * @param owner
	 */
	public static void warningNotification(String content, Stage owner) {
		ControlFXUtil.showNotification(AlertType.WARNING, Pos.BOTTOM_RIGHT, "警告", content, owner);
	}

	/**
	 * Error Notification
	 * 
	 * @param title
	 * @param content
	 * @param owner
	 */
	public static void errorNotification(String content, Stage owner) {
		ControlFXUtil.showNotification(AlertType.ERROR, Pos.BOTTOM_RIGHT, "错误", content, owner);
	}

	/**
	 * 
	 * alert dialog with owner
	 * 
	 * @param type AlertType.(CONFIRMATION|INFORMATION|ERROR|WARNING)
	 * @param title
	 * @param header
	 * @param content
	 * @param ico
	 * @param owner
	 */
	public static Alert alert(AlertType type, String title, String header, String content, Image ico, Stage owner) {
		Alert alert = new Alert(type);
		alert.setTitle(title);
		alert.setHeaderText(header);
		alert.setContentText(content);
		if (ico != null) {
			// Get the Stage.
			Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
			// Add a custom icon.
			stage.getIcons().add(ico);
		}
		if (owner != null) {
			// owner
			alert.initOwner(owner);
		}

		alert.show();
		return alert;
	}

	/**
	 * Simple Information dialog
	 * 
	 * @param content
	 * @param ico
	 * @param owner
	 * @return
	 */
	public static Alert infoDialog(String content, Image ico, Stage owner) {
		return infoDialog(null, content, ico, owner);
	}

	/**
	 * Simple Information dialog
	 * 
	 * @param content
	 * @param ico
	 * @param owner
	 * @return
	 */
	public static Alert infoDialog(String header, String content, Image ico, Stage owner) {
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("信息");
		alert.setHeaderText(header);
		alert.setContentText(content);
		if (ico != null) {
			// Get the Stage.
			Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
			// Add a custom icon.
			stage.getIcons().add(ico);
		}
		if (owner != null) {
			// owner
			alert.initOwner(owner);
		}

		alert.show();
		return alert;
	}

	/**
	 * Simple warning dialog
	 * 
	 * @param content
	 * @param ico
	 * @param owner
	 * @return
	 */
	public static Alert warningDialog(String content, Image ico, Stage owner) {
		Alert alert = new Alert(AlertType.WARNING);
		alert.setTitle("警告");
		alert.setHeaderText(null);
		alert.setContentText(content);
		if (ico != null) {
			// Get the Stage.
			Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
			// Add a custom icon.
			stage.getIcons().add(ico);
		}
		if (owner != null) {
			// owner
			alert.initOwner(owner);
		}

		alert.show();
		return alert;
	}

	/**
	 * Simple warning dialog(Waiting)
	 * 
	 * @param content
	 * @param ico
	 * @param owner
	 * @return
	 */
	public static Alert warningDialogWaiting(String content, Image ico, Stage owner) {
		Alert alert = new Alert(AlertType.WARNING);
		alert.setTitle("警告");
		alert.setHeaderText(null);
		alert.setContentText(content);
		if (ico != null) {
			// Get the Stage.
			Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
			// Add a custom icon.
			stage.getIcons().add(ico);
		}
		if (owner != null) {
			// owner
			alert.initOwner(owner);
		}

		alert.showAndWait();
		return alert;
	}

	/**
	 * Simple error dialog
	 * 
	 * @param content
	 * @param ico
	 * @param owner
	 * @return
	 */
	public static Alert errorDialog(String content, Image ico, Stage owner) {
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle("错误");
		alert.setHeaderText(null);
		alert.setContentText(content);
		if (ico != null) {
			// Get the Stage.
			Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
			// Add a custom icon.
			stage.getIcons().add(ico);
		}
		if (owner != null) {
			// owner
			alert.initOwner(owner);
		}

		alert.show();
		return alert;
	}

	/**
	 * Simple error dialog(Waiting)
	 * 
	 * @param content
	 * @param ico
	 * @param owner
	 * @return
	 */
	public static Alert errorDialogWaiting(String content, Image ico, Stage owner) {
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle("错误");
		alert.setHeaderText(null);
		alert.setContentText(content);
		if (ico != null) {
			// Get the Stage.
			Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
			// Add a custom icon.
			stage.getIcons().add(ico);
		}
		if (owner != null) {
			// owner
			alert.initOwner(owner);
		}

		alert.showAndWait();
		return alert;
	}

	/**
	 * Input Dialog
	 * 
	 * @param title
	 * @param header
	 * @param content
	 * @param ico
	 * @param owner
	 * @return
	 */
	@Deprecated
	public static String inputDialog(String title, String header, String content, Image ico, Stage owner) {
		TextInputDialog dialog = new TextInputDialog();
		dialog.setTitle(title);
		dialog.setHeaderText(header);
		dialog.setContentText(content);
		if (ico != null) {
			// Get the Stage.
			Stage stage = (Stage) dialog.getDialogPane().getScene().getWindow();
			// Add a custom icon.
			stage.getIcons().add(ico);
		}

		if (owner != null) {
			// owner
			dialog.initOwner(owner);
		}
		// Traditional way to get the response value.
		Optional<String> result = dialog.showAndWait();
		if (result.isPresent()) {
			//			System.out.println("Scan Input: " + result.get());
			return result.get();
		} else {
			return null;
		}
	}

	public static String inputDialog(String header, String content, boolean isRequired, Image ico, Stage owner) {
		TextInputDialog dialog = new TextInputDialog();
		dialog.setTitle("Input Dialog");
		dialog.setHeaderText("\n" + header + "\n\n");
		dialog.setContentText(content);

		//set dialog Image
		writeImageView.setFitWidth(64);
		writeImageView.setFitHeight(64);
		dialog.setGraphic(writeImageView);

		if (ico != null) {
			// Get the Stage.
			Stage stage = (Stage) dialog.getDialogPane().getScene().getWindow();
			// Add a custom icon.
			stage.getIcons().add(ico);
		}

		if (owner != null) {
			// owner
			dialog.initOwner(owner);
		}

		//required text input
		if (isRequired) {
			// Enable/Disable login button depending on whether a username was entered.
			Node loginButton = dialog.getDialogPane().lookupButton(ButtonType.OK);
			loginButton.setDisable(true);
			TextField text = (TextField) dialog.getDialogPane().lookup(".text-field");
			// Do some validation (using the Java 8 lambda syntax).
			text.textProperty().addListener((observable, oldValue, newValue) -> {
				loginButton.setDisable(newValue.trim().isEmpty());
			});
		}

		// Traditional way to get the response value.
		Optional<String> result = dialog.showAndWait();
		if (result.isPresent()) {
			return result.get();
		} else {
			return null;
		}
	}

	/**
	 * Input Note Dialog
	 * 
	 * @param header
	 * @param isRequired : flag to write something
	 * @param ico
	 * @param owner
	 * @return
	 */
	public static String inputNoteDialog(String header, boolean isRequired, Image ico, Stage owner) {
		// Create the custom dialog.
		Dialog<String> dialog = new Dialog<>();
		dialog.setTitle("Input Dialog");
		dialog.setHeaderText("\n" + header + "\n\n");

		//set dialog Image
		writeImageView.setFitWidth(64);
		writeImageView.setFitHeight(64);
		dialog.setGraphic(writeImageView);

		// Set the Stage title Icon.
		if (ico != null) {
			// Get the Stage.
			Stage stage = (Stage) dialog.getDialogPane().getScene().getWindow();
			// Add a custom icon.
			stage.getIcons().add(ico);
		}

		if (owner != null) {
			// owner
			dialog.initOwner(owner);
		}

		// Set the button types.
		ButtonType loginButtonType = new ButtonType("确认", ButtonData.OK_DONE);
		dialog.getDialogPane().getButtonTypes().addAll(loginButtonType, ButtonType.CANCEL);

		// Create the username and password labels and fields.
		GridPane grid = new GridPane();
		grid.setHgap(10);
		grid.setVgap(10);
		grid.setPadding(new Insets(10, 20, 10, 10));

		TextArea textArea = new TextArea();
		textArea.setPrefRowCount(6);

		//put into content pane
		grid.add(new Label("请输入："), 1, 0);
		grid.add(textArea, 1, 1);

		textArea.setMaxWidth(Double.MAX_VALUE);
		textArea.setMaxHeight(Double.MAX_VALUE);
		GridPane.setVgrow(textArea, Priority.ALWAYS);
		GridPane.setHgrow(textArea, Priority.ALWAYS);

		//required text input
		if (isRequired) {
			// Enable/Disable login button depending on whether a username was entered.
			Node loginButton = dialog.getDialogPane().lookupButton(loginButtonType);
			loginButton.setDisable(true);
			// Do some validation (using the Java 8 lambda syntax).
			textArea.textProperty().addListener((observable, oldValue, newValue) -> {
				loginButton.setDisable(newValue.trim().isEmpty());
			});
		}

		// set dialog pane content
		dialog.getDialogPane().setContent(grid);

		// Request focus on the username field by default.
		Platform.runLater(() -> textArea.requestFocus());

		// Convert the result to input value when the ok button clicked.
		dialog.setResultConverter(dialogButton -> {
			if (dialogButton == loginButtonType) {
				return textArea.getText();
			}
			return null;
		});

		Optional<String> result = dialog.showAndWait();

		if (result.isPresent()) {//click confirm
			return result.get();
		} else {//click cancel or close
			return null;
		}
	}

	/**
	 * Simple Confirm Dialog
	 * 
	 * @param content
	 * @param ico
	 * @param owner
	 * @return true/false
	 */
	public static boolean confirmDialog(String content, Image ico, Stage owner) {
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("确认");
		alert.setHeaderText(null);
		alert.setContentText(content);
		if (ico != null) {
			// Get the Stage.
			Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
			// Add a custom icon.
			stage.getIcons().add(ico);
		}

		if (owner != null) {
			alert.initOwner(owner);
		}

		Optional<ButtonType> confirmResult = alert.showAndWait();
		if (confirmResult.get() == ButtonType.OK) {
			// ... user chose OK
			System.out.println("Confirm");
			return true;
		} else {
			// ... user chose CANCEL or closed the dialog
			System.out.println("CANCEL");
			return false;
		}
	}

	/**
	 * confirm dialog with owner
	 * 
	 * @param title
	 * @param header
	 * @param content
	 * @return
	 */
	public static Optional<ButtonType> confirmDialog(String title, String header, String content, Image ico, Stage owner) {
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle(title);
		alert.setHeaderText(header);
		alert.setContentText(content);
		if (ico != null) {
			// Get the Stage.
			Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
			// Add a custom icon.
			stage.getIcons().add(ico);
		}

		if (owner != null) {
			alert.initOwner(owner);
		}

		return alert.showAndWait();
		/*Optional<ButtonType> confirmResult = alert.showAndWait();
		if (confirmResult.get() == ButtonType.OK) {
			// ... user chose OK
			System.out.println("Confirm");
		} else {
			// ... user chose CANCEL or closed the dialog
			System.out.println("CANCEL");
		}*/
	}

	/**
	 * show choice dialog
	 * 
	 * @param title
	 * @param header
	 * @param content
	 * @param choices
	 * @param defaultValue
	 * @return
	 */
	public static String choiceDialog(String title, String header, String content, List<String> choices, String defaultValue, Stage owner) {
		ChoiceDialog<String> dialog = new ChoiceDialog<>(defaultValue, choices);
		dialog.setTitle(title);
		dialog.setHeaderText(header);
		dialog.setContentText(content);

		if (owner != null) {
			dialog.initOwner(owner);
		}

		Optional<String> result = dialog.showAndWait();
		if (result.isPresent()) {
			//			System.out.println("Your choice: " + result.get());
			return result.get();
		} else {
			return null;
		}
	}

	/**
	 * show exception dialog
	 * 
	 * @param header
	 * @param e
	 */
	public static void exceptionDialog(String header, Exception e, Stage owner) {
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle("Exception Dialog");
		alert.setHeaderText(header);
		alert.setContentText("查看并记录错误信息，与技术人员联系解决！");

		if (owner != null) {
			alert.initOwner(owner);
		}

		// Create expandable Exception.
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		e.printStackTrace(pw);
		String exceptionText = sw.toString();

		Label label = new Label("The exception stacktrace was:");

		TextArea textArea = new TextArea(exceptionText);
		textArea.setEditable(false);
		textArea.setWrapText(true);

		textArea.setMaxWidth(Double.MAX_VALUE);
		textArea.setMaxHeight(Double.MAX_VALUE);
		GridPane.setVgrow(textArea, Priority.ALWAYS);
		GridPane.setHgrow(textArea, Priority.ALWAYS);

		GridPane expContent = new GridPane();
		expContent.setMaxWidth(Double.MAX_VALUE);
		expContent.add(label, 0, 0);
		expContent.add(textArea, 0, 1);

		// Set expandable Exception into the dialog pane.
		alert.getDialogPane().setExpandableContent(expContent);

		alert.showAndWait();
	}

	public static void exceptionDialog(Exception ex, Stage owner) {
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle("System Exception");
		alert.setHeaderText("系统异常！");

		//check exception type
		if (ex != null && ex.getLocalizedMessage() != null && ex.getLocalizedMessage().contains("Violation of PRIMARY KEY constraint")) {
			alert.setContentText("数据库保存失败！失败原因：数据库主键ID已存在！");
		} else {
			alert.setContentText(ex.getLocalizedMessage());
		}

		if (owner != null) {
			alert.initOwner(owner);
		}

		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		ex.printStackTrace(pw);
		String exceptionText = sw.toString();

		Label label = new Label("异常详细信息如下：");

		TextArea textArea = new TextArea(exceptionText);
		textArea.setEditable(false);
		textArea.setWrapText(true);

		textArea.setMaxWidth(Double.MAX_VALUE);
		textArea.setMaxHeight(Double.MAX_VALUE);
		GridPane.setVgrow(textArea, Priority.ALWAYS);
		GridPane.setHgrow(textArea, Priority.ALWAYS);

		GridPane expContent = new GridPane();
		expContent.setMaxWidth(Double.MAX_VALUE);
		expContent.add(label, 0, 0);
		expContent.add(textArea, 0, 1);

		alert.getDialogPane().setExpandableContent(expContent);
		alert.showAndWait();
	}

}
