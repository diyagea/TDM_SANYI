package app.util;

public class PathUtil {
	/*Project Root Path*/
	public static final String _ROOT_PATH = CommonUtil.getRootPath();

	/*System Image*/
	public static final String _IMG_PATH = "file:/" + _ROOT_PATH + "/resources/img/";

	/*CAD*/
	public static final String _CAD_URI = "file:/" + _ROOT_PATH + "/CAD/";

	public static final String LifeCardPath = _ROOT_PATH + "/ToolLifeCards/";

	public static final String qrCodePath = _ROOT_PATH + "/QRCodePath/";

}
