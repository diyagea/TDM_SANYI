package app.util;

import java.lang.reflect.Field;

import org.controlsfx.glyphfont.FontAwesome;
import org.controlsfx.glyphfont.Glyph;

import cn.hutool.log.Log;
import cn.hutool.log.LogFactory;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.Duration;

public class FXUtil {

	//Logger
	private static final Log logger = LogFactory.get();

	public static final String _IMG_PATH = "file:resources/img/";
	public static final String _MENU_IMG_PATH = "file:resources/img/menu/";
	public static final String _ICON_IMG_PATH = "file:resources/img/icon/";

	/**
	 * Get FOREIGN KEY ID（eg: id-name） if "" => return null
	 * 
	 * @return
	 */
	public static String getForeignKeyID(String val) {
		try {
			if (ValidatorUtil.isEmpty(val)) {
				return null;
			} else {
				return val.split("-")[0];
			}
		} catch (Exception e) {
			logger.info("Get Foreign Key ID Error! val={}", val);
			return null;
		}
	}

	/**
	 * Load Glyph Icon
	 * 
	 * @param icon
	 * @return
	 */
	public static Glyph getIcon(FontAwesome.Glyph icon) {
		return new Glyph("FontAwesome", icon);
	}

	/**
	 * Load Image Object, Default Path=（file:resources/img/）
	 * 
	 * @param imgName
	 * @return
	 */
	public static Image getImg(String imgName) {
		return getImg(_IMG_PATH, imgName);
	}
	
	/**
	 * Load Image Object
	 * 
	 * @param imgName
	 * @return
	 */
	public static Image getImg(String path, String imgName) {
		Image img = null;
		try {
			img = new Image(path + imgName);
		} catch (Exception e) {
			logger.error(e, "Load Image [{}] Error.", imgName);
		}
		return img;
	}


	/**
	 * show tooltip
	 * 
	 * @param content
	 * @param autoHide
	 * @param owner
	 */
	public static void showTooltip(String content, boolean autoHide, Stage owner) {
		Tooltip tooltip = new Tooltip();
		tooltip.setText(content);
		tooltip.setAutoHide(autoHide);
		tooltip.centerOnScreen();
		tooltip.show(owner);
	}

	/**
	 * Set Tooltip Show Time
	 * 
	 * @param tooltip
	 * @param time (ms)
	 */
	public static void setTooltipTiming(Tooltip tooltip, double time) {
		try {
			Field fieldBehavior = tooltip.getClass().getDeclaredField("BEHAVIOR");
			fieldBehavior.setAccessible(true);
			Object objBehavior = fieldBehavior.get(tooltip);

			Field fieldTimer = objBehavior.getClass().getDeclaredField("activationTimer");
			fieldTimer.setAccessible(true);
			Timeline objTimer = (Timeline) fieldTimer.get(objBehavior);

			objTimer.getKeyFrames().clear();
			objTimer.getKeyFrames().add(new KeyFrame(new Duration(time)));
		} catch (Exception e) {
			logger.error(e, "Set Tooltip Time Error!");
		}
	}

	/**
	 * Table Column Install with a ToolTip
	 * @param column
	 */
	public static <T, V> void addTooltipToColumn(TableColumn<T, V> column) {

		Callback<TableColumn<T, V>, TableCell<T, V>> existingCellFactory = column.getCellFactory();

		column.setCellFactory(c -> {
			TableCell<T, V> cell = existingCellFactory.call(c);

			Tooltip tooltip = new Tooltip();
			tooltip.setFont(new Font("Arial", 12));
			//set toolTip style ans times
			setTooltipTiming(tooltip, 500);

			// can use arbitrary binding here to make text depend on cell
			// in any way you need:
			tooltip.textProperty().bind(cell.itemProperty().asString());
			tooltip.setWrapText(true);
			tooltip.setPrefWidth(300);

			cell.setTooltip(tooltip);
			return cell;
		});
	}

}
