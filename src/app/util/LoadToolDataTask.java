package app.util;

import app.MainApp;
import app.service.TDMService;
import cn.hutool.cron.task.Task;
import cn.hutool.log.Log;
import cn.hutool.log.LogFactory;

public class LoadToolDataTask implements Task {
	private static final Log logger = LogFactory.get();
	private MainApp mainApp;

	public LoadToolDataTask(MainApp mainApp) {
		this.mainApp = mainApp;
	}

	@Override
	public void execute() {
		try {
			logger.info("Start Task Loading Issued Tool Data!");
			int successCount = TDMService.me.updateTableData();
			if (successCount > 0) {
				mainApp.getTLMCtrl().handleSearch();
			}
			logger.info("End Task Loading Issued Tool Data!");
		} catch (Exception e) {
			logger.error(e, "Load ToolData Task Error!");
		}
	}
}
