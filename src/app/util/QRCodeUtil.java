package app.util;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Hashtable;

import javax.imageio.ImageIO;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.common.BitMatrix;

import cn.hutool.log.Log;
import cn.hutool.log.LogFactory;

public class QRCodeUtil {
	//Logger Object
	private static final Log logger = LogFactory.get();
	private static final String format = "png";// 二维码的图片格式 

	/**
	 * @discription 生成二维码图片，返回Image对象
	 * @param content 二维码文字， path 存储路径
	 * @return
	 * @throws Exception
	 */
	public static Image writeQrCodeContent(String fileName, String content, String path) {
		Image im = null;
		try {
			int width = 300; // 二维码图片宽度 
			int height = 300; // 二维码图片高度 

			Hashtable<EncodeHintType, String> hints = new Hashtable<EncodeHintType, String>();
			hints.put(EncodeHintType.CHARACTER_SET, "utf-8"); // 内容所使用字符集编码 

			BitMatrix bitMatrix = new MultiFormatWriter().encode(content, BarcodeFormat.QR_CODE, width, height, hints);

			File filePath = new File(path);
			if (!filePath.exists()) {
				filePath.mkdirs();
			}
			// 生成二维码 
			String fileNamePath = path + File.separator + fileName + "." + format;
			File outputFile = new File(fileNamePath);
			if (!outputFile.exists()) {
				outputFile.createNewFile();
			}
			writeToFile(bitMatrix, format, outputFile);

			File file = new File(fileNamePath);
			InputStream is = new FileInputStream(file);
			BufferedImage bi;
			bi = ImageIO.read(is);
			im = bi;
		} catch (Exception e) {
			logger.error(e);
		}
		return im;
	}

	private static final int BLACK = 0xFF000000;
	private static final int WHITE = 0xFFFFFFFF;

	public static BufferedImage toBufferedImage(BitMatrix matrix) {
		int width = matrix.getWidth();
		int height = matrix.getHeight();
		BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				image.setRGB(x, y, matrix.get(x, y) ? BLACK : WHITE);
			}
		}
		return image;
	}

	public static void writeToFile(BitMatrix matrix, String format, File file) throws IOException {
		BufferedImage image = toBufferedImage(matrix);
		if (!ImageIO.write(image, format, file)) {
			throw new IOException("Could not write an image of format " + format + " to " + file);
		}
	}

	public static void writeToStream(BitMatrix matrix, String format, OutputStream stream) throws IOException {
		BufferedImage image = toBufferedImage(matrix);
		if (!ImageIO.write(image, format, stream)) {
			throw new IOException("Could not write an image of format " + format);
		}
	}

	public static void main(String[] args) {
		writeQrCodeContent("TEST", "123456", "C:/Users/diyagea/Desktop/");
	}
}
