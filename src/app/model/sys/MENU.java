package app.model.sys;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import app.model.base.sys.BaseMENU;

/**
 * System Menu Class.
 * 
 * TYPE:0=tree, 1=leaf(view), 2=button
 * 
 * STATE:0=Disable, 1=Enable, 2=Enable(Expanded Tree)
 */
@SuppressWarnings("serial")
public class MENU extends BaseMENU<MENU> {
	public static final MENU dao = new MENU().dao();
	public static final String TABLE_NAME = "SYS_MENU";

	private List<MENU> subMenuList;

	public MENU() {
		super();
	}

	public MENU(int pid, int menuID, String module, String menuIndex, String menuName, String fxml, String image, int type) {
		setMENUID(menuID);
		setPID(pid);
		setMODULE(module);
		setMENUINDEX(menuIndex);
		setMENUNAME(menuName);
		setFXML(fxml);
		setIMAGE(image);
		setTYPE(type); //0: NON-SHOW, 1:Expanded Tree, 2:Tree Leaf Item
	}

	//For Sub Menu List
	public void addSubMenu(MENU subMenu) {
		if (subMenuList == null) {
			subMenuList = new ArrayList<>();
		}
		subMenuList.add(subMenu);
	}

	//For Sub Menu List
	public void addSubMenus(Collection<MENU> subMenus) {
		if (subMenuList == null) {
			subMenuList = new ArrayList<>();
		}
		subMenuList.addAll(subMenus);
	}

	public List<MENU> getSubMenuList() {
		return subMenuList;
	}

	public void setSubMenuList(List<MENU> subMenuList) {
		this.subMenuList = subMenuList;
	}

	@Override
	public String toString() {
		return getMENUNAME();
	}

	@Override
	public boolean equals(Object o) {
		if (super.equals(o)) {
			MENU m = (MENU) o;
			if (this.getSubMenuList() != null) {
				if (this.getSubMenuList().equals(m.getSubMenuList())) {
					return true;
				}
			} else {
				if (m.getSubMenuList() == null) {
					return true;
				}
			}
		}
		return false;
	}

}
