package app.util;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import cn.hutool.core.util.XmlUtil;

/**
 * Load XML Data Util
 * @author diyagea
 *
 */
public class XmlDataUtil {
	public static final String _root_Path = System.getProperty("user.dir").replace("\\", "/") + "/";
	public static final String _xml_data_path = "/resources/data/";

	/**
	 * get Xml Data List
	 */
	public static List<String> getDataList(String name) {
		File xmlFile = new File(_root_Path + _xml_data_path + name + ".xml");
		List<String> dataList = new ArrayList<>();
		Document doc = XmlUtil.readXML(xmlFile);
		Element root = XmlUtil.getRootElement(doc);
		List<Element> tagList = XmlUtil.getElements(root, null);
		for (Element e : tagList) {
			dataList.add(e.getTextContent());
		}

		return dataList;
	}

}
