package app.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.jfinal.plugin.activerecord.Db;

import app.model.sys.MENU;
import app.model.sys.USER;
import app.model.sys.USERMENU;
import app.util.CommonUtil;
import app.util.FXUtil;
import app.util.ValidatorUtil;
import cn.hutool.core.lang.Validator;
import cn.hutool.core.util.CharsetUtil;
import cn.hutool.crypto.SecureUtil;
import cn.hutool.crypto.symmetric.SymmetricAlgorithm;
import cn.hutool.crypto.symmetric.SymmetricCrypto;
import cn.hutool.log.Log;
import cn.hutool.log.LogFactory;
import javafx.scene.control.CheckBoxTreeItem;
import javafx.scene.control.TreeItem;
import javafx.scene.image.ImageView;

public class SystemService {
	//Logger Object
	private static final Log logger = LogFactory.get();
	//Service Self Object
	public static final SystemService me = new SystemService();

	/**
	 * query by menu index
	 * @param menuIndex
	 * @return
	 */
	public MENU queryMenuByIndex(String menuIndex) {
		return MENU.dao.findFirst("SELECT * FROM " + MENU.TABLE_NAME + " WHERE MENUINDEX=? ", menuIndex);
	}

	/**
	 * Generate System Menu Tree (RootLayout)
	 * 
	 * @return TreeItem<MENU>
	 */
	public TreeItem<MENU> generateMenuTree(MENU menu) {
		TreeItem<MENU> treeItem = new TreeItem<MENU>(menu);

		//check & set image
		if (Validator.isNotEmpty(menu.getIMAGE())) {
			ImageView icon = new ImageView(FXUtil.getImg(FXUtil._MENU_IMG_PATH, menu.getIMAGE()));
			treeItem.setGraphic(icon);
		}

		//state:2 => expand tree menu
		if (menu.getSTATE() == 2) {
			treeItem.setExpanded(true);
		}

		//loop sub list
		if (menu.getSubMenuList() != null && !menu.getSubMenuList().isEmpty()) {
			for (MENU subMenu : menu.getSubMenuList()) {
				TreeItem<MENU> subTreeItem = generateMenuTree(subMenu);
				treeItem.getChildren().add(subTreeItem);
			}
		}
		return treeItem;
	}

	/**
	 * Generate System Check Menu Tree (Menu Setting)
	 * 
	 * @return CheckBoxTreeItem<MENU>
	 */
	public CheckBoxTreeItem<MENU> generateCheckMenuTree(MENU menu) {
		if (menu.getSTATE() == 0) {
			return null;
		}

		CheckBoxTreeItem<MENU> treeItem = new CheckBoxTreeItem<MENU>(menu);
		treeItem.setExpanded(true);

		//loop sub list
		if (menu.getSubMenuList() != null && !menu.getSubMenuList().isEmpty()) {
			for (MENU subMenu : menu.getSubMenuList()) {
				CheckBoxTreeItem<MENU> subTreeItem = generateCheckMenuTree(subMenu);
				treeItem.getChildren().add(subTreeItem);
			}
		}
		return treeItem;
	}

	/*
	 * load all menu
	 */
	public MENU loadRootMenu() {
		MENU rootMenu = MENU.dao.findById(1);
		loadSubMenuList(rootMenu);
		return rootMenu;
	}

	/*
	 * load sub Menu list(Loop Method)
	 */
	private MENU loadSubMenuList(MENU menu) {
		List<MENU> subList = MENU.dao.find("SELECT * FROM " + MENU.TABLE_NAME + " WHERE PID = ? AND STATE > 0 ORDER BY POS, MENUID ", menu.getMENUID());
		//build MENU with subList
		for (MENU m : subList) {
			menu.addSubMenu(m);
			loadSubMenuList(m);
		}
		return menu;
	}

	/**
	 * load Root Menu By User
	 * 
	 * @param username
	 * @return
	 */
	public MENU loadRootUserMenu(String username) {
		List<USERMENU> userMenuList = this.loadUserMenuList(username);
		List<MENU> menuTreeList = new ArrayList<>();
		for (USERMENU userMenu : userMenuList) {
			MENU uMenu = MENU.dao.findById(userMenu.getMENUID());
			menuTreeList.add(loadParentMenuTree(uMenu));
		}

		//do loop get merge Root　Menu
		return mergeRootUserMenu(menuTreeList);
	}

	/**
	 * load Parent Menu Tree (Loop Method)
	 * 
	 * @param menu
	 * @return
	 */
	private MENU loadParentMenuTree(MENU menu) {
		//check if root menu
		if (menu.getPID() == 0) {
			return menu;
		}
		//get parent menu
		MENU pMenu = MENU.dao.findById(menu.getPID());
		//add sub menu
		pMenu.addSubMenu(menu);

		//loop
		return loadParentMenuTree(pMenu);
	}

	/**
	 * merge Menu Tree(merge into one tree)
	 * 
	 * @param menu
	 * @param username
	 * @return
	 */
	public MENU mergeRootUserMenu(List<MENU> menuTreeList) {
		//leaf menu item
		if (menuTreeList == null) {
			return null;
		}

		//remove list
		List<MENU> removeList = new ArrayList<>();

		//Model Menu Object(for compare)
		MENU modelMenu = null;
		for (int i = 0; i < menuTreeList.size(); i++) {
			if (modelMenu == null) {
				modelMenu = menuTreeList.get(i);
				continue;
			}

			MENU currMenu = menuTreeList.get(i);
			if (currMenu.getMENUID() == modelMenu.getMENUID()) {
				modelMenu.addSubMenus(currMenu.getSubMenuList());
				removeList.add(currMenu);
			} else {
				mergeRootUserMenu(modelMenu.getSubMenuList());
				modelMenu = currMenu;
			}
		}
		//[trigger loop]last model or not change model
		if (modelMenu != null) {
			mergeRootUserMenu(modelMenu.getSubMenuList());
		}

		//remove multiple menu
		menuTreeList.removeAll(removeList);

		return modelMenu;
	}

	/*
	 * load USERMENU list
	 */
	public List<USERMENU> loadUserMenuList(String username) {
		return USERMENU.dao.find("SELECT * FROM " + USERMENU.TABLE_NAME + " WHERE USERNAME = ? ", username);
	}

	/*********** USERMENU **********/
	/*
	 * To Build Checkbox MenuTree
	 */
	public Map<Integer, USERMENU> getUserMenuMap(String username) {
		Map<Integer, USERMENU> userMenuMap = new HashMap<>();
		List<USERMENU> userMenuList = USERMENU.dao.find("SELECT * FROM " + USERMENU.TABLE_NAME + " WHERE USERNAME = ? ", username);
		for (USERMENU um : userMenuList) {
			userMenuMap.put(um.getMENUID(), um);
		}
		return userMenuMap;
	}

	/*
	 * get User Menu Auth Map
	 */
	public Map<String, USERMENU> getUserAuthMap(String username) {
		Map<String, USERMENU> userMenuMap = new HashMap<>();
		List<USERMENU> userMenuList = USERMENU.dao.find("SELECT * FROM " + USERMENU.TABLE_NAME + " WHERE USERNAME = ? ", username);
		for (USERMENU um : userMenuList) {
			userMenuMap.put(um.getMENUINDEX(), um);
		}
		return userMenuMap;
	}

	/**
	 * create USERMENU Data line(insert to database)
	 * 
	 * @param selectedMenus
	 * @param username
	 */
	public void createUserMenu(List<USERMENU> selectedMenus, String username) {
		if (selectedMenus == null || selectedMenus.size() == 0) {
			return;
		}

		//delete old UserMenu
		Db.update("DELETE FROM " + USERMENU.TABLE_NAME + " WHERE USERNAME = ? ", username);

		//Set Timestamp
		String timestamp = CommonUtil.getUnixTimestamp() + "";

		//Create New UserMenu
		for (USERMENU um : selectedMenus) {
			um.setTIMESTAMP(timestamp);
			um.save();
		}
	}

	/*********** USER **********/
	/*
	 * Get All User(Choose dialog)
	 */
	public List<USER> getAllUserList() {
		List<USER> userList = USER.dao.find("SELECT * FROM " + USER.TABLE_NAME + " WHERE TYPE > 0 ");
		for (USER u : userList) {
			byte[] key = u.getSECRET();
			SymmetricCrypto aes = new SymmetricCrypto(SymmetricAlgorithm.AES, key);
			String passwordText = aes.decryptStr(u.getPASSWORD(), CharsetUtil.CHARSET_UTF_8);
			u.setPASSWORD(passwordText);
		}
		return userList;
	}

	/*
	 * Create USER
	 */
	public boolean createUser(USER user, List<USERMENU> selectedMenus, Map<String, Object> dataMap) {
		try {
			//check exist (username)
			int userCount = Db.queryInt("SELECT COUNT(1) FROM " + USER.TABLE_NAME + " WHERE USERNAME = ? ", user.getUSERNAME());
			if (userCount >= 1) {
				dataMap.put("ERROR", "失败原因：此登录名已存在！");
				return false;
			}

			//check exist (if pincode input)
			if (ValidatorUtil.isNotEmpty(user.getPINCODE())) {
				int pincodeCount = Db.queryInt("SELECT COUNT(1) FROM " + USER.TABLE_NAME + " WHERE PINCODE = ? ", user.getPINCODE());
				if (pincodeCount >= 1) {
					dataMap.put("ERROR", "失败原因：此用户PIN码已存在！");
					return false;
				}
			}

			//Set Comp other data
			user.setTIMESTAMP(CommonUtil.getUnixTimestamp() + "");
			user.setTYPE(1);//TYPE:1=normal user

			//encrypt password
			String password = user.getPASSWORD();
			//generate random key and do encrypt
			byte[] key = SecureUtil.generateKey(SymmetricAlgorithm.AES.getValue()).getEncoded();
			SymmetricCrypto aes = new SymmetricCrypto(SymmetricAlgorithm.AES, key);
			String passwordHex = aes.encryptHex(password);
			//Set Password and Secert
			user.setPASSWORD(passwordHex);
			user.setSECRET(key);

			//save USER
			if (!user.save()) {
				dataMap.put("ERROR", "失败原因：数据库保存失败！");
				return false;
			}

			//Save UserMenu
			createUserMenu(selectedMenus, user.getUSERNAME());

			return true;
		} catch (Exception e) {
			logger.error(e, "Create User Error! Username='{}'", user.getUSERNAME());
			dataMap.put("ERROR", "失败原因：数据库异常！");
			return false;
		}

	}

	/*
	 * update USER
	 */
	public boolean updateUser(USER user, List<USERMENU> selectedMenus, Map<String, Object> dataMap) {
		try {
			//check if username exist
			USER temp = USER.dao.findById(user.getUSERNAME());
			if (temp == null) {
				dataMap.put("ERROR", "失败原因：用户不存在，请先创建！");
				return false;
			}

			//check exist if pincode changed
			if (temp.getPINCODE() != null && ValidatorUtil.isNotEmpty(user.getPINCODE())) {
				if (!temp.getPINCODE().equals(user.getPINCODE())) {
					int pincodeCount = Db.queryInt("SELECT COUNT(1) FROM " + USER.TABLE_NAME + " WHERE PINCODE = ? ", user.getPINCODE());
					if (pincodeCount >= 1) {
						dataMap.put("ERROR", "失败原因：此用户PIN码已存在！");
						return false;
					}
				}
			}

			//Set other data
			user.setTIMESTAMP(CommonUtil.getUnixTimestamp() + "");

			//Encrypt password
			String password = user.getPASSWORD();
			//Get key
			byte[] key = user.getSECRET();
			//do encrypt
			SymmetricCrypto aes = new SymmetricCrypto(SymmetricAlgorithm.AES, key);
			String passwordHex = aes.encryptHex(password);
			//Set Password
			user.setPASSWORD(passwordHex);

			//update COMP
			if (!user.update()) {
				dataMap.put("ERROR", "失败原因：数据库保存失败.");
				return false;
			}

			//Save UserMenu
			createUserMenu(selectedMenus, user.getUSERNAME());

			return true;
		} catch (Exception e) {
			dataMap.put("ERROR", "失败原因：数据库异常！");
			logger.error(e, "Save User Error! Username='{}'", user.getUSERNAME());
			return false;
		}
	}

	/*
	 * Delete USER
	 */
	public boolean deleteUser(String username, Map<String, Object> dataMap) {
		try {
			Db.update("DELETE FROM " + USERMENU.TABLE_NAME + " WHERE USERNAME = ? ", username);
			if (!USER.dao.deleteById(username)) {
				dataMap.put("ERROR", "数据库操作失败！");
				return false;
			}
			return true;
		} catch (Exception e) {
			dataMap.put("ERROR", "失败原因：数据库异常！");
			logger.error(e, "Delete User Error! Username='{}'", username);
			return false;
		}
	}

	/**
	 * User Login (Account)
	 * 
	 * @param username
	 * @param password
	 * @param dataMap
	 * @return
	 */
	public boolean doLogin(String username, String password, Map<String, Object> dataMap) {
		try {
			//query USER
			USER user = USER.dao.findById(username);

			//check null
			if (user == null) {
				dataMap.put("ERROR", "用户不存在，请检查用户名是否输入有误！");
				return false;
			}

			//check state
			if (user.getSTATE() == 0) {
				dataMap.put("ERROR", "用户被禁用，请联系管理员！");
				return false;
			}

			//check password
			byte[] key = user.getSECRET();
			SymmetricCrypto aes = new SymmetricCrypto(SymmetricAlgorithm.AES, key);
			//decrypt password
			String passwordText = aes.decryptStr(user.getPASSWORD(), CharsetUtil.CHARSET_UTF_8);
			user.setPASSWORD(passwordText);
			if (!passwordText.equals(password)) {
				dataMap.put("ERROR", "登录密码不正确，请重试！");
				return false;
			}

			//return back USER
			dataMap.put("USER", user);

			return true;
		} catch (Exception e) {
			logger.error(e, "Do Login Error!");
			dataMap.put("ERROR", "系统登录异常！");
			return false;
		}
	}

	/**
	 * 
	 * User Login (Pincode)
	 * 
	 * @param pincode
	 * @param dataMap
	 * @return
	 */
	public boolean doLogin(String pincode, Map<String, Object> dataMap) {
		try {
			//query USER
			USER user = USER.dao.findFirst("SELECT * FROM " + USER.TABLE_NAME + " WHERE PINCODE = ? ", pincode);

			//check null
			if (user == null) {
				dataMap.put("ERROR", "用户不存在，请检查用户PIN码是否输入有误！");
				return false;
			}

			//check state
			if (user.getSTATE() == 0) {
				dataMap.put("ERROR", "用户被禁用，请联系管理员！");
				return false;
			}

			//return back USER
			dataMap.put("USER", user);

			return true;
		} catch (Exception e) {
			logger.error(e, "Do Login Error!");
			dataMap.put("ERROR", "系统登录异常！");
			return false;
		}
	}

}
