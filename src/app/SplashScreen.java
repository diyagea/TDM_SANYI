package app;

import app.util.FXUtil;
import javafx.application.Preloader;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class SplashScreen extends Preloader {
	private ProgressBar loadProgress;
	private Label progressText;
	private Stage stage;

	private int SPLASH_WIDTH = 240;//image width + 20
	private int SPLASH_HEIGHT = 220;

	private Scene createPreloaderScene() {
		//ImageView splash = new ImageView(new Image("http://fxexperience.com/wp-content/uploads/2010/06/logo.png"));
		Image img = FXUtil.getImg("Logo.png");
		ImageView splash = new ImageView(img);
		
		loadProgress = new ProgressBar();
		loadProgress.setPrefWidth(SPLASH_WIDTH - 20);

		progressText = new Label();
		progressText.setText("Application Start Now!");
		progressText.setAlignment(Pos.CENTER);

		VBox splashLayout = new VBox();
		splashLayout.setAlignment(Pos.CENTER);
		splashLayout.getChildren().addAll(splash, loadProgress, progressText);
		splashLayout.setStyle("-fx-padding: 5; -fx-background-color: cornsilk; -fx-border-width:5; -fx-border-color: linear-gradient(to bottom, chocolate, derive(chocolate, 50%));");
		splashLayout.setEffect(new DropShadow());

		Scene splashScene = new Scene(splashLayout);

		stage.setAlwaysOnTop(true);
		stage.initStyle(StageStyle.UNDECORATED);
		stage.setScene(splashScene);
		final Rectangle2D bounds = Screen.getPrimary().getBounds();
		stage.setX(bounds.getMinX() + bounds.getWidth() / 2 - SPLASH_WIDTH / 2);
		stage.setY(bounds.getMinY() + bounds.getHeight() / 2 - SPLASH_HEIGHT / 2);

		return splashScene;
	}

	public void start(Stage stage) throws Exception {
		this.stage = stage;
		stage.setScene(createPreloaderScene());
		stage.show();
	}

	@Override
	public void handleProgressNotification(ProgressNotification pn) {
		//loadProgress.setProgress(pn.getProgress());
	}

	@Override
	public void handleStateChangeNotification(StateChangeNotification evt) {
		//Loading now.
		if (evt.getType() == StateChangeNotification.Type.BEFORE_LOAD) {
			progressText.setText("Loading Data!");
		}
		
		//Initializing now.
		if (evt.getType() == StateChangeNotification.Type.BEFORE_INIT) {
			progressText.setText("Initializing Data!");
		}

		//Init Finished, Now run Start Method, hide SplashScreen
		if (evt.getType() == StateChangeNotification.Type.BEFORE_START) {
			progressText.setText("Finished!");
			stage.hide();
		}
	}
}