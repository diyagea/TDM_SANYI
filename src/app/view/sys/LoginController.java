package app.view.sys;

import java.util.HashMap;
import java.util.Map;

import app.MainApp;
import app.service.SystemService;
import cn.hutool.log.Log;
import cn.hutool.log.LogFactory;
import javafx.fxml.FXML;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

public class LoginController {
	//Logger Object
	private static final Log logger = LogFactory.get();
	private Map<String, Object> dataMap;

	// main ref
	private MainApp mainApp;

	private Stage dialogStage;
	private boolean loginFLag = false;

	private int loginType = 0;
	@FXML
	private Hyperlink loginTypeLink;
	@FXML
	private Label msgLabel;
	@FXML
	private Label usercodeLabel;
	@FXML
	private Label usernameLabel;
	@FXML
	private Label passwordLabel;
	@FXML
	private TextField usernameInput;
	@FXML
	private PasswordField passwordInput;
	@FXML
	private PasswordField pincodeInput;

	@FXML
	private void initialize() {
		dataMap = new HashMap<>();
		pincodeInput.setVisible(false);
	}

	@FXML
	private void changeLoginType() {
		if (loginType == 0) {//input login
			loginType = 1;

			loginTypeLink.setText("帐号登录");
			pincodeInput.setVisible(true);
			usercodeLabel.setVisible(true);
			//focus
			pincodeInput.requestFocus();
			
			usernameLabel.setVisible(false);
			passwordLabel.setVisible(false);
			usernameInput.setVisible(false);
			passwordInput.setVisible(false);
		} else {//scan login
			loginType = 0;

			loginTypeLink.setText("扫码登录");
			pincodeInput.setVisible(false);
			usercodeLabel.setVisible(false);

			usernameLabel.setVisible(true);
			passwordLabel.setVisible(true);
			usernameInput.setVisible(true);
			passwordInput.setVisible(true);
			
			//focus
			usernameInput.requestFocus();
		}
	}

	/**
	 * press keyborad on username testfield.
	 */
	@FXML
	private void doEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ESCAPE) {
			//ESC close
			dialogStage.close();
		}
		if (event.getCode() == KeyCode.ENTER) {
			//press Enter, to focus next textfield password
			passwordInput.requestFocus();
		}
	}

	/**
	 * Login Action press keyborad on password testfield.
	 */
	@FXML
	private void doEnter2(KeyEvent event) {
		//ESC - close dialog
		if (event.getCode() == KeyCode.ESCAPE) {
			dialogStage.close();
		}

		//Enter - do login action
		if (event.getCode() == KeyCode.ENTER) {
			doLogin();
		}
	}

	@FXML
	private void clickLogin() {
		doLogin();
	}

	private void doLogin() {
		if (loginType == 0) {//input user account login
			//input username & password
			String username = usernameInput.getText();
			String password = passwordInput.getText();

			if (SystemService.me.doLogin(username, password, dataMap)) {
				loginFLag = true;

				//Put USER into Application Session
				mainApp.getSession().put("USER", dataMap.get("USER"));
				
				//get user auth map
				mainApp.getSession().put("AUTHMAP", SystemService.me.getUserAuthMap(username));

				dialogStage.close();
			} else {
				//DialogUtil.alert(AlertType.ERROR, "登录失败", null, "失败原因："+dataMap.get("ERROR"), mainApp.getLogo(), dialogStage);
				msgLabel.setText((String) dataMap.get("ERROR"));
				logger.info("User Input Account Login Fail: username=[" + username + "]");
				return;
			}

		} else if (loginType == 1) {//scan user code login
			//input pincode text
			String pincode = pincodeInput.getText();
			if (SystemService.me.doLogin(pincode, dataMap)) {
				loginFLag = true;

				//Put USER into Application Session
				mainApp.getSession().put("USER", dataMap.get("USER"));

				dialogStage.close();
			} else {
				//DialogUtil.alert(AlertType.ERROR, "登录失败", null, "失败原因："+dataMap.get("ERROR"), mainApp.getLogo(), dialogStage);
				msgLabel.setText((String) dataMap.get("ERROR"));
				logger.info("User Scan Pincode Login Fail: pincode=[" + pincode + "]");
				return;
			}

		}
	}

	/**
	 * Sets the stage of this dialog.
	 * 
	 * @param dialogStage
	 */
	public void setDialogStage(Stage dialogStage) {
		this.dialogStage = dialogStage;
	}

	/**
	 * Returns true if the user clicked OK, false otherwise.
	 * 
	 * @return
	 */
	public boolean isOkClicked() {
		return loginFLag;
	}

	/*
	 * MainApp Ref
	 */
	public void setMainApp(MainApp mainApp) {
		this.mainApp = mainApp;
	}

}
