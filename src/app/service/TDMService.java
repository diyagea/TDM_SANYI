package app.service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;

import app.model.hmi.TOOLDATA;
import app.model.hmi.TOOLDATALOG;
import app.util.CommonUtil;
import app.util.ValidatorUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.log.Log;
import cn.hutool.log.LogFactory;

public class TDMService {
	//Logger Object
	private static final Log logger = LogFactory.get();
	//Service Self Object
	public static final TDMService me = new TDMService();

	/**
	 * Get All Machine ID
	 * 
	 * @return
	 */
	public List<String> getMachineIDs() {
		return Db.query("SELECT COSTUNIT FROM TMS_COSTUNIT WHERE TYPE = 2");
	}

	/**
	 * Query All Related Tool List IDs
	 * 
	 * @param toolID
	 * @return
	 */
	public List<String> getToolListIDs(String toolID) {
		String sql = "SELECT DISTINCT TL.LISTID FROM TDM_LIST TL LEFT JOIN TDM_LISTLIST TLL ON TL.LISTID = TLL.LISTID WHERE TLL.ID=? AND TLL.IDTYPE=2 ";
		return Db.query(sql, toolID);
	}

	/**
	 * Load Tool Data
	 * 
	 * @param listID
	 * @param toolID
	 * @param dataMap
	 * @return
	 */
	public boolean loadToolData(String listID, String toolID, Map<String, Object> dataMap) {
		try {
			List<Record> toolList = Db.find("SELECT * FROM TDM_LISTLIST WHERE LISTID=? AND ID=? ", listID, toolID);

			//刀具不存在刀具列表中
			if (toolList.size() == 0) {
				dataMap.put("ERROR", "刀具列表[" + listID + "]未包含此刀具[" + toolID + "]");
				return false;
			}

			//多个刀具存在刀具列表中，让用户选择刀具位置
			if (toolList.size() > 1) {
				List<String> posList = new ArrayList<>();
				for (Record r : toolList) {
					posList.add(r.getStr("LISTLISTPOS"));
				}
				dataMap.put("POSLIST", posList);
				return false;
			}

			//只有一个刀具存在刀具列表
			Record listList = toolList.get(0);
			return checkListList(listList, dataMap);
		} catch (Exception e) {
			dataMap.put("ERROR", "系统异常！");
			logger.error(e);
			return false;
		}
	}

	/**
	 * Load Tool Data By Pos
	 * 
	 * @param listID
	 * @param toolID
	 * @param pos
	 * @param dataMap
	 * @return
	 */
	public boolean loadToolData(String listID, String toolID, String pos, Map<String, Object> dataMap) {
		Record listList = Db.findFirst("SELECT * FROM TDM_LISTLIST WHERE LISTID=? AND ID=? AND LISTLISTPOS=? ", listID, toolID, pos);
		return checkListList(listList, dataMap);
	}

	/**
	 * Check LISTLIST Model
	 * 
	 * @param listList
	 * @param dataMap
	 * @return
	 */
	private boolean checkListList(Record listList, Map<String, Object> dataMap) {
		try {
			//check toolNumber
			String toolNumber = listList.getStr("TOOLNUMBER");
			if (toolNumber == null) {
				dataMap.put("ERROR", "刀具T号未设置！");
				return false;
			}
			dataMap.put("TOOLNUMBER", toolNumber);

			//check techoNoPos
			String techNoPos = listList.getStr("TECHNOPOS");
			if (techNoPos == null) {
				dataMap.put("ERROR", "刀具技术参数未设置！");
				return false;
			}

			//check lifeData
			return checkLifeData(listList.getStr("ID"), techNoPos, dataMap);
		} catch (Exception e) {
			dataMap.put("ERROR", "系统异常！");
			logger.error(e);
			return false;
		}
	}

	/**
	 * Check Life Data
	 * 
	 * @param toolID
	 * @param techNoPos
	 * @param dataMap
	 * @return
	 */
	private boolean checkLifeData(String toolID, String techNoPos, Map<String, Object> dataMap) {
		try {
			//query TechnoList
			Record technoList = Db.findFirst("SELECT * FROM TDM_TOOLTECHNOLIST WHERE TOOLID=? AND TOOLTECHNOPOS=? AND TOOLTECHNOLISTPOS=1", toolID, techNoPos);
			String toolLife = technoList.getStr("TOOLLIFE");
			if (toolLife == null) {//ToolLifeTime is null
				toolLife = technoList.getStr("TOOLLIFECOUNT");
			}
			if (toolLife == null) {//ToolLifeCount is null
				toolLife = technoList.getStr("TOOLLIFEWAY");
			}
			if (toolLife == null) {//ToolLifeWay is null
				//No ToolLife Data
				dataMap.put("ERROR", "刀具寿命未设置！");
				return false;
			}

			String warningLife = technoList.getStr("WARNINGLIFE");
			if (warningLife == null) {
				//No ToolLife Data
				dataMap.put("ERROR", "刀具预警寿命未设置！");
				return false;
			}

			dataMap.put("TOOLLIFE", toolLife);
			dataMap.put("WARNINGLIFE", warningLife);

			return true;
		} catch (Exception e) {
			dataMap.put("ERROR", "系统异常！");
			logger.error(e);
			return false;
		}
	}

	public List<TOOLDATA> getIssueOutToolDatas(String machineInfo, Integer type, String toolInfo, LocalDate startDate, LocalDate endDate) {
		StringBuilder sql = new StringBuilder("SELECT TOP 1000 * FROM " + TOOLDATA.TABLE_NAME + " WHERE ISREAD=0 ");
		if (type != null) {
			sql.append(" AND TYPE=" + type);
		}
		if (machineInfo != null && machineInfo.length() > 0) {
			sql.append(" AND MACHINEID LIKE '%" + machineInfo + "%' ");
		}
		if (toolInfo != null && toolInfo.length() > 0) {
			sql.append(" AND TOOLID LIKE '%" + toolInfo + "%' ");
		}
		if (startDate != null) {
			sql.append(" AND DATETIME >= '" + startDate.toString() + "' ");
		}
		if (endDate != null) {
			sql.append(" AND DATETIME <= '" + endDate.toString() + "' ");
		}
		sql.append(" ORDER BY DATETIME DESC ");
		System.out.println(sql.toString());
		return TOOLDATA.dao.find(sql.toString());
	}

	/**
	 * Query ToolData
	 * 
	 * @param guid
	 * @return
	 */
	public TOOLDATA queryToolDataByGUID(String guid) {
		return TOOLDATA.dao.findFirst("SELECT * FROM " + TOOLDATA.TABLE_NAME + " WHERE GUID=? ", guid);
	}

	/**
	 * Update TOOLDATA
	 * 
	 * @param toolData
	 * @param dataMap
	 * @return
	 */
	public boolean updateToolData(TOOLDATA toolData, Map<String, Object> dataMap) {
		try {
			toolData.setUPDATETIME(new Date());
			if (!toolData.update()) {
				dataMap.put("ERROR", "数据库保存失败！");
				return false;
			}
			return true;
		} catch (Exception e) {
			dataMap.put("ERROR", "系统异常！");
			logger.error(e);
			return false;
		}
	}

	/**
	 * Update Table Data
	 */
	public int updateTableData() {
		long startTime = DateUtil.offsetDay(new Date(), -30).getTime() / 1000;
		long endTime = System.currentTimeMillis() / 1000;
		//query LGM history data
		String sql = "SELECT CANCELNR, BOOKTYPE, COSTUNITFROM, COSTUNITTO,LISTID, ID, TT.NAME, BOOKTIMESTAMP FROM LGM_HISTORYBASE LH JOIN TMS_COSTUNIT TC ON TC.COSTUNIT = LH.COSTUNITFROM AND TC.WORKPLACE = LH.WORKPLACEFROM JOIN TMS_COSTUNIT TC2 ON TC2.COSTUNIT = LH.COSTUNITTO AND TC2.WORKPLACE = LH.WORKPLACETO JOIN TDM_TOOL TT ON TT.TOOLID = LH.ID WHERE (BOOKTYPE = -3 OR BOOKTYPE = -6) AND LH.IDTYPE = 2 AND TC.TYPE = 1 AND TC2.TYPE = 2 AND BOOKTIMESTAMP>=? AND BOOKTIMESTAMP<=? ORDER BY LH.BOOKTIMESTAMP DESC";
		List<Record> datas = Db.find(sql, startTime, endTime);
		int successCount = 0;
		int errorCount = 0;
		int ignoreCount = 0;
		for (Record r : datas) {
			//check exist data
			int count = Db.queryInt("SELECT COUNT(1) FROM " + TOOLDATA.TABLE_NAME + " WHERE CANCELNR=? ", r.getStr("CANCELNR"));
			if (count == 0) {
				TOOLDATA toolData = new TOOLDATA();
				toolData.setCANCELNR(r.getStr("CANCELNR"));
				toolData.setMACHINEID(r.getStr("COSTUNITTO"));
				toolData.setTOOLID(r.getStr("ID"));
				toolData.setTOOLNAME(r.getStr("NAME"));
				toolData.setDATETIME(CommonUtil.getDateByTimestamp(r.getStr("BOOKTIMESTAMP")));
				toolData.setUPDATETIME(new Date());
				toolData.setTYPE(0);
				toolData.setISREAD(0);

				//issue list tool
				String listID = r.getStr("LISTID");
				if (ValidatorUtil.isNotEmpty(listID)) {
					Map<String, Object> dataMap = new HashMap<>();
					if (loadToolData(listID, toolData.getTOOLID(), dataMap)) {
						String toolNumber = (String) dataMap.get("TOOLNUMBER");
						String toolLife = (String) dataMap.get("TOOLLIFE");
						String warningLife = (String) dataMap.get("WARNINGLIFE");
						toolData.setTOOLNUMBER(toolNumber);
						toolData.setDEFINELIFE(new BigDecimal(toolLife));
						toolData.setREALLIFE(new BigDecimal(toolLife));
						toolData.setWARNINGLIFE(new BigDecimal(warningLife));
					}
				}

				if (toolData.save()) {
					successCount++;
				} else {
					errorCount++;
				}
			} else {
				ignoreCount++;
			}
		}

		logger.info("Load Last 30 days TDM History Record : " + datas.size());
		logger.info("Insert Success Record : " + successCount);
		logger.info("Insert Error Record : " + errorCount);
		logger.info("Exist and Ignore Record : " + ignoreCount);
		return successCount;
	}

	public List<TOOLDATALOG> queryToolDataLogs(String machineInfo, String toolInfo, LocalDate startDate, LocalDate endDate) {
		StringBuilder sql = new StringBuilder("SELECT TOP 1000 * FROM " + TOOLDATALOG.TABLE_NAME + " WHERE 1=1 ");
		if (machineInfo != null && machineInfo.length() > 0) {
			sql.append(" AND MACHINEID LIKE '%" + machineInfo + "%' ");
		}
		if (toolInfo != null && toolInfo.length() > 0) {
			sql.append(" AND TOOLID LIKE '%" + toolInfo + "%' ");
		}
		if (startDate != null) {
			sql.append(" AND DATETIME >= '" + startDate.toString() + "' ");
		}
		if (endDate != null) {
			sql.append(" AND DATETIME <= '" + endDate.toString() + "' ");
		}
		sql.append(" ORDER BY DATETIME DESC ");
		System.out.println(sql.toString());
		return TOOLDATALOG.dao.find(sql.toString());
	}

	public void addLog(int type, String machineID, String toolID, String note) {
		new TOOLDATALOG().setTYPE(type).setMACHINEID(machineID).setTOOLID(toolID).setNOTE(note).setDATETIME(new Date()).save();
	}
}
