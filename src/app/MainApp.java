package app;

import java.util.HashMap;
import java.util.Map;

import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.activerecord.dialect.SqlServerDialect;
import com.jfinal.plugin.druid.DruidPlugin;
import com.sun.javafx.application.LauncherImpl;

import app.model._MappingKit;
import app.service.TDMService;
import app.util.CommonUtil;
import app.util.DialogUtil;
import app.util.FXUtil;
import app.util.LoadToolDataTask;
import app.view.ToolLifeManagementController;
import app.view.base.BaseController;
import app.view.base.BaseDialogController;
import cn.hutool.cron.CronUtil;
import cn.hutool.log.Log;
import cn.hutool.log.LogFactory;
import cn.hutool.setting.Setting;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Screen;
import javafx.stage.Stage;

/**
 * Smartool Application Main Class
 * 
 * @author Allen Wang
 *
 */
public class MainApp extends Application {

	//Logger
	private static final Log logger = LogFactory.get();

	//Application Session Map
	private Map<String, Object> session = new HashMap<>();

	private ToolLifeManagementController tlmCtrl;

	//Setting
	private Setting setting = null;

	//Database Plugin
	private DruidPlugin druidPlugin;
	private boolean databaseFlag = false;

	//Main Stage
	private Stage primaryStage;

	//Logo ico
	private Image logo;

	/**
	 * Application Run Main Method
	 */
	public static void main(String[] args) {
		LauncherImpl.launchApplication(MainApp.class, SplashScreen.class, args);
		//launch(args);
	}

	/**
	 * Application Init loading Method
	 */
	@Override
	public void init() throws Exception {
		setting = CommonUtil.getSetting("config.setting");
		session.put("setting", setting);
		logo = FXUtil.getImg(setting.getStr("AppIco"));
		databaseFlag = startDBPlugin();
		startTimeTask();
	}

	/**
	 * Application Start Method
	 */
	@Override
	public void start(Stage primaryStage) {

		//Start DB Plugin and Check DB connection 
		if (!databaseFlag) {
			DialogUtil.errorDialog("数据库连接失败，请检查配置文件是否正确！", null, null);
			return;
		}

		try {
			//App Windows Setting
			//App Stage Ref
			this.primaryStage = primaryStage;
			//set width / height values to be 75% of users screen resolution
			Rectangle2D screenBounds = Screen.getPrimary().getVisualBounds();
			primaryStage.setWidth(screenBounds.getWidth() * 0.75);
			primaryStage.setHeight(screenBounds.getHeight() * .75);

			//App Windows Min Size Setting
			primaryStage.setMinWidth(setting.getDouble("AppMinWidth"));
			primaryStage.setMinHeight(setting.getDouble("AppMinHeight"));

			//Maximized Setting
			primaryStage.setMaximized(setting.getBool("AppInitMaximized"));

			// Set the application icon.
			this.primaryStage.getIcons().add(logo);

			//set close Event Handle
			/*primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
				@Override
				public void handle(WindowEvent event) {
					Optional<ButtonType> confirmResult = DialogUtil.confirmDialog("提示", null, "确认关闭刀具寿命管理吗？", logo, null);
					if (confirmResult.get() == ButtonType.OK) {
						//APP Will Close
					} else {
						//APP Cancel To Close
						event.consume();
					}
				}
			});*/

			//Title Info
			String databaseInfo = setting.getStr("url") + ":" + setting.getStr("port") + "/" + setting.getStr("database");
			//Set App Title
			primaryStage.setTitle("RFID寿命管理 - [ " + databaseInfo + " ]");

			//update table
			TDMService.me.updateTableData();

			//Init Root layout frame
			initRootLayout();
		} catch (Exception e) {
			logger.error(e);
		}
	}

	/**
	 * Application Start loading Method
	 */
	@Override
	public void stop() throws Exception {
		//do something need to stop
		if (druidPlugin != null) {
			druidPlugin.stop();
		}
		System.exit(0);
	}

	/**
	 * Start Database Plugin
	 */
	public boolean startDBPlugin() {
		try {
			//init Database connection pool
			String jdbcUrl = "jdbc:sqlserver://" + setting.getStr("url") + ":" + setting.getStr("port") + ";databaseName=" + setting.getStr("database");
			druidPlugin = new DruidPlugin(jdbcUrl, setting.getStr("username"), setting.getStr("password"));
			ActiveRecordPlugin activeRecordPlugin = new ActiveRecordPlugin(druidPlugin);

			//set SQL Dialect
			activeRecordPlugin.setDialect(new SqlServerDialect());

			//Mapping Model Object
			_MappingKit.mapping(activeRecordPlugin);

			// 与 jfinal web 环境唯一的不同是要手动调用一次相关插件的start()方法
			if (druidPlugin.start() && activeRecordPlugin.start()) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			logger.error(e, "Connect Database Error!");
			return false;
		}
	}

	/**
	 * Start Time Task
	 */
	private void startTimeTask() {
		LoadToolDataTask loadTask = new LoadToolDataTask(this);//transfer mainApp to task
		CronUtil.schedule("* * * * *", loadTask);
		CronUtil.start();
	}

	/**
	 * Initializes the root layout and App frame.
	 */
	private void initRootLayout() {
		try {
			// Load root layout from fxml file.
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("view/ToolLifeManagement.fxml"));
			Pane rootLayout = loader.load();

			// Show the scene containing the root layout.
			Scene scene = new Scene(rootLayout);
			primaryStage.setScene(scene);

			// Give the controller access to the main app.
			tlmCtrl = loader.getController();
			tlmCtrl.setMainApp(this);

			primaryStage.show();
		} catch (Exception e) {
			logger.error(e, "Initializes ToolLifeManagement Fxml Error!");
		}

	}

	/********************** FXML Method *************************/

	/**
	 * Load FXML Layout Pane
	 * 
	 * @param fxml
	 * @return Pane of FXML
	 */
	public Node loadFxmlPane(String module, String fxml) {
		Pane view = null;
		try {
			// Load root layout from fxml file.
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("view/" + module + "/" + fxml));
			view = (Pane) loader.load();

			// Give the controller access to the main app.
			BaseController controller = loader.getController();
			controller.setMainApp(this);

		} catch (Exception e) {
			logger.error(e, "Load Fxml Pane Error, fxmlName='{}'", fxml);
		}
		return view;
	}

	/**
	 * Load FXML Layout Pane
	 * 
	 * @param fxml
	 * @return Pane of FXML
	 */
	public Node loadFxmlPane(String module, String fxml, Map<String, Object> dataMap) {
		Pane view = null;
		try {
			// Load root layout from fxml file.
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("view/" + module + "/" + fxml));
			view = (Pane) loader.load();

			// Give the controller access to the main app.
			BaseController controller = loader.getController();
			controller.setMainApp(this);
			controller.setDataMap(dataMap);

		} catch (Exception e) {
			logger.error(e, "Load Fxml Pane Error, fxmlName='{}'", fxml);
		}
		return view;
	}

	/**
	 * Load FXML Layout Dialog
	 * 
	 * @return boolean if click OK or Cancel
	 */
	public boolean loadFxmlDialog(String module, String fxml, String title, Map<String, Object> dataMap) {
		try {
			// Load the fxml file and create a new stage for the popup dialog.
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("view/" + module + "/" + fxml));
			Pane page = (Pane) loader.load();

			// Create the dialog Stage.
			Stage dialogStage = new Stage();
			dialogStage.setTitle(title);
			dialogStage.initModality(Modality.APPLICATION_MODAL);
			dialogStage.initOwner(primaryStage);
			dialogStage.getIcons().add(logo);
			Scene scene = new Scene(page);
			dialogStage.setScene(scene);
			dialogStage.setResizable(false);
			dialogStage.centerOnScreen();

			// controller setting
			BaseDialogController controller = loader.getController();
			controller.setDialogStage(dialogStage);
			controller.setMainApp(this);
			controller.setDataMap(dataMap);

			// Show the dialog and wait until the user closes it
			dialogStage.showAndWait();

			return controller.isOkClicked();
		} catch (Exception e) {
			logger.error(e, "Load Fxml Dialog Error, fxmlName='{}'", fxml);
			return false;
		}
	}

	/********************** GET&SET *************************/

	/**
	 * Get logo image
	 */
	public Image getLogo() {
		return logo;
	}

	/**
	 * Get the main stage.
	 */
	public Stage getPrimaryStage() {
		return primaryStage;
	}

	/**
	 * Get App Session Object
	 */
	public Map<String, Object> getSession() {
		return session;
	}

	/*
	 * Get Setting
	 */
	public Setting getSetting() {
		return setting;
	}

	public ToolLifeManagementController getTLMCtrl() {
		return tlmCtrl;
	}

}